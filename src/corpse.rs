use bevy::{ecs::system::SystemId, prelude::*, reflect::TypeUuid, sprite::Anchor};
use bevy_asepritesheet::{core::load_spritesheet, sprite::Spritesheet};
use bevy_dynamic_gravity_2d::core::physics::dynamic_gravity::DynamicGravity;
use bevy_xpbd_2d::prelude::*;

use crate::{
    controller::player::PlayerId,
    math_utils::*,
    transform_link::{OwnedBy, TransformLinker},
    weapons::{basic_projectile::Knockback, Projectile},
    HealthChecking, PhysicsLayers, ProjectileHitTarget, HERO_COLORS,
};

// System Ids: -------------------------------------------------------------------------------------

#[derive(Resource)]
pub struct CorpseSystems {
    pub cleanup: SystemId,
}

// Plugin: -----------------------------------------------------------------------------------------

pub struct CorpsePlugin;

impl Plugin for CorpsePlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = CorpseSystems {
            cleanup: app.world.register_system(cleanup_corpses),
        };
        app.init_asset::<CorpseAsset>()
            .insert_resource(sys_ids)
            .insert_resource(CorpseAssetResource::default())
            .insert_resource(CorpseGfxResource::default())
            .add_systems(Startup, load_corpses)
            .add_systems(
                FixedUpdate,
                (
                    apply_deferred,
                    spawn_corpses,
                    apply_deferred,
                    apply_corpse_proj_hits,
                )
                    .chain()
                    .after(HealthChecking)
                    .before(PhysicsSet::Prepare),
            )
            .add_systems(PostUpdate, create_corpse_part_gfx);
    }
}

// Asset: ------------------------------------------------------------------------------------------

#[derive(Asset, TypeUuid, TypePath, Default, Debug, Clone)]
#[uuid = "13161cdf-a7f0-4dbe-8f92-c3da3871fa7c"]
pub struct CorpseAsset {
    pub parts: Vec<Collider>,
    pub joints: Vec<CorpseJointConfig>,
    pub poses: Vec<CorpsePose>,
}

#[derive(Debug, Clone)]
pub struct CorpseJointConfig {
    pub joint: RevoluteJoint,
    pub piece_index_1: usize,
    pub piece_index_2: usize,
}

#[derive(Resource, Default, Debug, Clone)]
pub struct CorpseAssetResource {
    pub hero_corpse: Handle<CorpseAsset>,
}

#[derive(Resource, Debug, Clone)]
pub struct CorpseGfxResource {
    pub hero_corpse_ent: Entity,
}

impl Default for CorpseGfxResource {
    fn default() -> Self {
        Self {
            hero_corpse_ent: Entity::PLACEHOLDER,
        }
    }
}

pub fn load_corpses(
    mut commands: Commands,
    mut corps_res: ResMut<CorpseAssetResource>,
    mut corpse_assets: ResMut<Assets<CorpseAsset>>,
    mut corpse_gfx_assets: ResMut<CorpseGfxResource>,
    asset_server: Res<AssetServer>,
) {
    let bund = load_spritesheet(
        &mut commands,
        &asset_server,
        "hero_corpse.sprite.json",
        Anchor::Center,
    );
    corpse_gfx_assets.hero_corpse_ent = commands.spawn(bund).id();

    corps_res.hero_corpse = corpse_assets.add(CorpseAsset {
        poses: [CorpsePose {
            positions_rotations: [
                // head
                (Vec2::new(0.0, 1.35), 0.0),
                // back
                (Vec2::new(0.0, 0.45), 0.0),
                (Vec2::new(0.0, 0.05), 0.0),
                // leg r
                (Vec2::new(0.35, -0.5), 0.0),
                (Vec2::new(0.35, -0.95), 0.0),
                // leg l
                (Vec2::new(-0.35, -0.5), 0.0),
                (Vec2::new(-0.35, -0.95), 0.0),
            ]
            .to_vec(),
        }]
        .to_vec(),
        parts: [
            // head
            Collider::ball(0.35),
            // back
            Collider::capsule(0.4, 0.3),
            Collider::capsule(0.4, 0.3),
            // leg r
            Collider::capsule(0.45, 0.2),
            Collider::capsule(0.45, 0.2),
            // leg l
            Collider::capsule(0.45, 0.2),
            Collider::capsule(0.45, 0.2),
        ]
        .to_vec(),
        joints: [
            // neck
            CorpseJointConfig {
                joint: RevoluteJoint::new(Entity::PLACEHOLDER, Entity::PLACEHOLDER)
                    .with_local_anchor_1(Vec2::new(0.0, -0.35))
                    .with_local_anchor_2(Vec2::new(0.0, 0.55))
                    .with_angle_limits(PI * -0.5, PI * 0.5)
                    .with_angular_velocity_damping(5.0),
                piece_index_1: 0,
                piece_index_2: 1,
            },
            // spine
            CorpseJointConfig {
                joint: RevoluteJoint::new(Entity::PLACEHOLDER, Entity::PLACEHOLDER)
                    .with_local_anchor_1(Vec2::new(0.0, -0.2))
                    .with_local_anchor_2(Vec2::new(0.0, 0.2))
                    .with_angle_limits(PI * -0.25, PI * 0.25)
                    .with_angular_velocity_damping(5.0),
                piece_index_1: 1,
                piece_index_2: 2,
            },
            // hip r
            CorpseJointConfig {
                joint: RevoluteJoint::new(Entity::PLACEHOLDER, Entity::PLACEHOLDER)
                    .with_local_anchor_1(Vec2::new(0.2, -0.4))
                    .with_local_anchor_2(Vec2::new(0.0, 0.225))
                    .with_angle_limits(PI * -0.5, PI * 0.5)
                    .with_angular_velocity_damping(2.0),
                piece_index_1: 2,
                piece_index_2: 3,
            },
            // hip l
            CorpseJointConfig {
                joint: RevoluteJoint::new(Entity::PLACEHOLDER, Entity::PLACEHOLDER)
                    .with_local_anchor_1(Vec2::new(-0.2, -0.4))
                    .with_local_anchor_2(Vec2::new(0.0, 0.225))
                    .with_angle_limits(PI * -0.5, PI * 0.5)
                    .with_angular_velocity_damping(2.0),
                piece_index_1: 2,
                piece_index_2: 5,
            },
            // knee r
            CorpseJointConfig {
                joint: RevoluteJoint::new(Entity::PLACEHOLDER, Entity::PLACEHOLDER)
                    .with_local_anchor_1(Vec2::new(0.0, -0.225))
                    .with_local_anchor_2(Vec2::new(0.0, 0.225))
                    .with_angle_limits(PI * -0.5, PI * 0.25)
                    .with_angular_velocity_damping(2.0),
                piece_index_1: 3,
                piece_index_2: 4,
            },
            // knee l
            CorpseJointConfig {
                joint: RevoluteJoint::new(Entity::PLACEHOLDER, Entity::PLACEHOLDER)
                    .with_local_anchor_1(Vec2::new(0.0, -0.225))
                    .with_local_anchor_2(Vec2::new(0.0, 0.225))
                    .with_angle_limits(PI * -0.5, PI * 0.25)
                    .with_angular_velocity_damping(2.0),
                piece_index_1: 5,
                piece_index_2: 6,
            },
        ]
        .to_vec(),
        ..Default::default()
    });
}

// Asset Handle Implementation: --------------------------------------------------------------------

pub trait CorpseEmitter {
    fn create_corpse_at(&self, commands: &mut Commands, pose: usize, spawner: Entity);
}

impl CorpseEmitter for Handle<CorpseAsset> {
    fn create_corpse_at(&self, commands: &mut Commands, pose_index: usize, spawner: Entity) {
        commands.spawn(CorpseBundle {
            owned_by: OwnedBy(spawner),
            corpse: Corpse {
                target_pose_index: pose_index,
                ..Default::default()
            },
            asset: self.clone(),
            ..Default::default()
        });
    }
}

#[derive(Debug, Default, Clone)]
pub struct CorpsePose {
    pub positions_rotations: Vec<(Vec2, f32)>,
}

// Corpse Core: ------------------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone)]
pub struct Corpse {
    pub owned_parts: Vec<Entity>,
    pub owned_joints: Vec<Entity>,
    pub target_pose_index: usize,
}

#[derive(Component, Default, Debug, Clone)]
struct ApplyHitsToCorpse;

fn spawn_corpses(
    mut commands: Commands,
    mut unspawned_corpse_query: Query<
        (Entity, &mut Corpse, &Handle<CorpseAsset>, &OwnedBy),
        With<UnspawnedCorpse>,
    >,
    spawner_query: Query<(&GlobalTransform, &LinearVelocity, &AngularVelocity, &Mass)>,
    corpse_assets: Res<Assets<CorpseAsset>>,
) {
    for (ent, mut corpse, asset_hdl, owner) in &mut unspawned_corpse_query {
        let Some(asset) = corpse_assets.get(asset_hdl) else {
            continue;
        };
        let Ok((owner_trans, owner_vel, owner_ang_vel, owner_mass)) = spawner_query.get(owner.0)
        else {
            warn!("CORPSE HAS NO OWNER D:");
            commands.entity(ent).despawn();
            continue;
        };
        // calculate the amount of mass each part should have
        let part_mass = owner_mass.0 / asset.parts.len() as f32;
        // add part entities
        for (i, part) in asset.parts.iter().enumerate() {
            let limb_pose = asset.poses[corpse.target_pose_index].positions_rotations[i];
            let part_rotation = owner_trans.rotation2d() + limb_pose.1;
            let part_pos = owner_trans
                .transform_point2(limb_pose.0)
                .extend(owner_trans.translation().z);
            let vel = owner_vel.0;
            let ang_vel = owner_ang_vel.0;
            let part_ent = commands
                .spawn((
                    OwnedBy(ent),
                    Transform::from_translation(part_pos).with_rotation2d(part_rotation),
                    part.clone(),
                    ExternalImpulse::default(),
                    PartialCorpsePartBundle {
                        part: CorpsePart(i),
                        velocity: LinearVelocity(vel),
                        angular_velocity: AngularVelocity(ang_vel),
                        mass: Mass(part_mass),
                        ..Default::default()
                    },
                ))
                .id();
            corpse.owned_parts.push(part_ent);
        }
        // add joints
        for joint_cfg in &asset.joints {
            let source_ent = corpse.owned_parts[joint_cfg.piece_index_1];
            let target_ent = corpse.owned_parts[joint_cfg.piece_index_2];
            let mut joint = joint_cfg.joint.clone();
            joint.entity1 = source_ent;
            joint.entity2 = target_ent;
            commands.spawn(joint);
        }
        // remove marker that says this corpse has not been spawned yet
        commands.entity(ent).remove::<UnspawnedCorpse>();
    }
}

fn apply_corpse_proj_hits(
    mut commands: Commands,
    corpse_query: Query<(Entity, &Corpse, &OwnedBy), With<ApplyHitsToCorpse>>,
    owner_query: Query<&ProjectileHitTarget>,
    parts_query: Query<(&GlobalTransform, &Collider)>,
    proj_query: Query<(&LinearVelocity, Option<&Knockback>), With<Projectile>>,
    mut impulse_query: Query<(Option<&CenterOfMass>, &mut ExternalImpulse)>,
) {
    for (ent, corpse, corpse_owner) in &corpse_query {
        commands.entity(ent).remove::<ApplyHitsToCorpse>();
        let Ok(target) = owner_query.get(corpse_owner.0) else {
            continue;
        };
        for hit in target.iter() {
            // (local point, part entity, distance squared)
            let mut closest_part: Option<(Vec2, Entity, f32)> = None;
            for part_ent in &corpse.owned_parts {
                let Ok((part_trans, col)) = parts_query.get(*part_ent) else {
                    continue;
                };
                // calculate the point projected onto the corpse part's collider
                let proj_point_local = part_trans.inverse_transform_point2(hit.1.point1);
                let point = Vec2::from_point2(
                    &col.shape()
                        .project_local_point(&proj_point_local.to_point2(), true)
                        .point,
                );
                let dist_sq = point.distance_squared(proj_point_local);
                if let Some(closest) = closest_part {
                    // test to see if the new point projection is closer, and if so use that
                    if dist_sq < closest.2 {
                        closest_part = Some((point, *part_ent, dist_sq));
                    }
                } else {
                    closest_part = Some((point, *part_ent, dist_sq));
                }
            }
            // apply the knockback to the closest corpse part based on the projectile stats
            if let Some(closest) = closest_part {
                let Ok((op_center_mass, mut impulse)) = impulse_query.get_mut(closest.1) else {
                    continue;
                };
                let Ok((proj_vel, Some(knockback))) = proj_query.get(hit.0) else {
                    continue;
                };
                let center_mass = op_center_mass.map_or(Vec2::ZERO, |c| c.0);
                impulse.apply_impulse_at_point(
                    knockback.0 * proj_vel.0.normalize_or_zero(),
                    closest.0,
                    center_mass,
                );
            }
        }
    }
}

fn create_corpse_part_gfx(
    mut commands: Commands,
    sheet_assets: Res<Assets<Spritesheet>>,
    corpse_gfx: Res<CorpseGfxResource>,
    new_parts: Query<(Entity, &CorpsePart, &OwnedBy), Added<CorpsePart>>,
    corpse_query: Query<&OwnedBy, With<Corpse>>,
    owner_query: Query<&PlayerId>,
    gfx_query: Query<&Handle<Spritesheet>>,
) {
    for (ent, part, part_owner) in &new_parts {
        let Ok(sheet_handle) = gfx_query.get(corpse_gfx.hero_corpse_ent) else {
            commands.entity(ent).despawn();
            continue;
        };
        let Some(sheet) = sheet_assets.get(sheet_handle) else {
            commands.entity(ent).despawn();
            continue;
        };
        let Ok(corpse_owner) = corpse_query.get(part_owner.0) else {
            continue;
        };
        let Ok(pid) = owner_query.get(corpse_owner.0) else {
            continue;
        };
        let t_col = HERO_COLORS[pid.0];
        commands.spawn((
            TransformLinker {
                linked_transform: ent,
            },
            SpriteSheetBundle {
                transform: Transform::from_scale(Vec3::new(0.1, 0.1, 1.0)),
                texture_atlas: sheet.atlas_handle().unwrap(),
                sprite: TextureAtlasSprite {
                    index: part.0,
                    color: Color::rgb(t_col.r() * 0.6, t_col.g() * 0.6, t_col.b() * 0.6),
                    ..Default::default()
                },
                ..Default::default()
            },
        ));
    }
}

// Corpse Bundle: ----------------------------------------------------------------------------------

#[derive(Bundle, Debug, Clone)]
struct CorpseBundle {
    pub owned_by: OwnedBy,
    pub corpse: Corpse,
    pub hits: ApplyHitsToCorpse,
    pub asset: Handle<CorpseAsset>,
    pub unspawned: UnspawnedCorpse,
}

impl Default for CorpseBundle {
    fn default() -> Self {
        Self {
            owned_by: OwnedBy(Entity::PLACEHOLDER),
            corpse: Default::default(),
            hits: Default::default(),
            asset: Default::default(),
            unspawned: Default::default(),
        }
    }
}

// Other Corpse Util: ------------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone)]
struct UnspawnedCorpse;

#[derive(Component, Default, Debug, Clone)]
pub struct CorpsePart(usize);

#[derive(Bundle, Debug, Clone)]
struct PartialCorpsePartBundle {
    pub part: CorpsePart,
    pub velocity: LinearVelocity,
    pub angular_velocity: AngularVelocity,
    pub glob_trans: GlobalTransform,
    pub rigid_body: RigidBody,
    pub mass: Mass,
    pub col_layers: CollisionLayers,
    pub dynamic_grav: DynamicGravity,
    pub sleep_disable: SleepingDisabled,
}

impl Default for PartialCorpsePartBundle {
    fn default() -> Self {
        Self {
            part: Default::default(),
            velocity: Default::default(),
            angular_velocity: Default::default(),
            glob_trans: Default::default(),
            rigid_body: RigidBody::Dynamic,
            mass: Default::default(),
            col_layers: CollisionLayers::new(
                [PhysicsLayers::FreshCorpses],
                [
                    PhysicsLayers::Terrain,
                    PhysicsLayers::Heros,
                    PhysicsLayers::Projectiles,
                ],
            ),
            dynamic_grav: Default::default(),
            sleep_disable: Default::default(),
        }
    }
}

fn cleanup_corpses(mut commands: Commands, corpse_query: Query<(Entity, &Corpse)>) {
    for (ent, corpse) in &corpse_query {
        commands.entity(ent).despawn();
        for joint_ent in &corpse.owned_joints {
            commands.entity(*joint_ent).despawn();
        }
        for part_ent in &corpse.owned_parts {
            commands.entity(*part_ent).despawn();
        }
    }
}
