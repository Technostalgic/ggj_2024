use bevy::{
    core_pipeline::clear_color::ClearColorConfig, ecs::system::SystemId, prelude::*,
    render::camera::Viewport, window::PrimaryWindow,
};
use rand::{thread_rng, Rng};

use crate::{
    controller::{
        player::{Player, PlayerId, Players},
        player_cam::{PlayerCamBundle, PlayerCamController},
    },
    corpse::{CorpseAssetResource, CorpseSystems},
    level::{LevelLoader, LevelSpawnPoints},
    score::Scores,
    weapons::{basic_gun::*, EquippedWeapon},
    HeroBundle,
};

// Consts: -----------------------------------------------------------------------------------------

const LEVEL_NAMES: [&str; 3] = ["diamond_arena", "caves", "urban_planet"];

// System Ids: -------------------------------------------------------------------------------------

#[derive(Resource)]
pub struct SetupSystemIds {
    pub restart_arena: SystemId,
    pub resize_cam_vps: SystemId,
}

// Setup Plugin: -----------------------------------------------------------------------------------

pub struct SetupPlugin;

impl Plugin for SetupPlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = SetupSystemIds {
            restart_arena: app.world.register_system(reset_arena),
            resize_cam_vps: app.world.register_system(resize_cam_viewports),
        };
        app.insert_resource(sys_ids)
            .insert_resource(PlayerCount::default())
            .insert_resource(CameraMode::Single);
    }
}

#[derive(Component, Debug, Clone, Copy)]
struct SetupObject;

#[derive(Resource, Default, Debug)]
pub struct PlayerCount {
    pub players: u8,
}

#[derive(Resource, Default, Debug)]
pub enum CameraMode {
    Single,
    #[default]
    SplitScreen,
}

// Systems: ----------------------------------------------------------------------------------------

fn reset_arena(
    mut commands: Commands,
    spawn_points: Res<LevelSpawnPoints>,
    setup_sys: Res<SetupSystemIds>,
    corpse_sys: Res<CorpseSystems>,
    plr_count: Res<PlayerCount>,
    mut scores: ResMut<Scores>,
    mut lvl_load: ResMut<LevelLoader>,
    setup_query: Query<Entity, With<SetupObject>>,
    cam_mode: Res<CameraMode>,
    corpses: Res<CorpseAssetResource>,
) {
    scores.reset_scores();
    commands.run_system(corpse_sys.cleanup);

    // remove all old setup objects
    for ent in &setup_query {
        commands.entity(ent).despawn();
    }

    // create players
    let mut cur_plrs = Vec::<Player>::new();
    let player_count = plr_count.players;
    for i in 0..(player_count as usize) {
        let plr = Player::new(i);
        cur_plrs.push(plr);
    }
    let plrs = Players {
        current_players: cur_plrs,
    };
    commands.insert_resource(plrs.clone());

    // create camera to clear the screen
    commands.spawn((
        SetupObject,
        Camera2dBundle {
            camera: Camera {
                order: -1,
                ..Default::default()
            },
            projection: OrthographicProjection {
                far: 2000.0,
                near: 1000.0,
                ..Default::default()
            },
            ..Default::default()
        },
    ));

    // create cameras
    match *cam_mode {
        CameraMode::SplitScreen => {
            for plr in plrs.current_players.iter() {
                commands.spawn((
                    SetupObject,
                    UiCameraConfig { show_ui: false },
                    PlayerCamBundle {
                        cam_bundle: Camera2dBundle {
                            projection: OrthographicProjection {
                                near: -1000.0,
                                far: 100.0,
                                scale: 0.05,
                                ..Default::default()
                            },
                            camera_2d: Camera2d {
                                clear_color: ClearColorConfig::None,
                            },
                            camera: Camera {
                                order: plr.id as isize,
                                ..Default::default()
                            },
                            ..Default::default()
                        },
                        player_cam_ctrl: PlayerCamController::new(plr.id),
                    },
                ));
            }
        }
        CameraMode::Single => {
            commands.spawn((
                SetupObject,
                UiCameraConfig { show_ui: false },
                PlayerCamBundle {
                    cam_bundle: Camera2dBundle {
                        projection: OrthographicProjection {
                            near: -1000.0,
                            far: 100.0,
                            scale: 0.05,
                            ..Default::default()
                        },
                        camera_2d: Camera2d {
                            clear_color: ClearColorConfig::None,
                        },
                        camera: Camera {
                            order: 1,
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                    player_cam_ctrl: PlayerCamController::new(255),
                },
            ));
        }
    }

    // create heros
    for plr in plrs.current_players.iter() {
        let mut wep_bund = MachineGunBundle::default();
        wep_bund.basic.player_id = PlayerId(plr.id);
        let wep_ent = commands.spawn((SetupObject, wep_bund)).id();
        let mut hero = HeroBundle::default();
        hero.corpse = corpses.hero_corpse.clone();
        hero.health.health = -1.0;
        hero.respawn.timer = hero.respawn.respawn_length - 0.5;
        hero.spatial.transform.translation = spawn_points.random().extend(0.0);
        hero.player_id.0 = plr.id;
        commands.spawn((
            SetupObject,
            EquippedWeapon::new(wep_ent, Vec2::new(0.0, 0.75), 1.5),
            hero,
        ));
    }

    // resize the viewports for the player cams
    commands.run_system(setup_sys.resize_cam_vps);

    // load random level
    lvl_load._load_level_at(
        "levels/".to_string()
            + LEVEL_NAMES[thread_rng().gen_range(0..LEVEL_NAMES.len())]
            + ".lvl.json",
    );
}

fn resize_cam_viewports(
    window_query: Query<&Window, With<PrimaryWindow>>,
    mut cam_query: Query<(&mut Camera, &PlayerCamController)>,
) {
    let cam_count = cam_query.iter().count();
    if cam_count <= 0 {
        return;
    }
    let window = window_query.single();
    let columns = (cam_count as f32).sqrt().ceil() as u32;
    let rows = if cam_count == 2 { 1 } else { columns };
    let size = UVec2::new(
        window.physical_width() / columns,
        window.physical_height() / rows,
    );
    for (mut cam, ctrl) in &mut cam_query {
        let plr_id = if ctrl.player_id == 255 {
            0
        } else {
            ctrl.player_id
        };
        let x_ind = (plr_id as u32) % columns;
        let y_ind = (plr_id as u32) / columns;
        let pos = UVec2::new(x_ind * size.x, y_ind * size.y);

        cam.viewport = Some(Viewport {
            physical_size: size,
            physical_position: pos,
            ..Default::default()
        });
    }
}
