mod controller;
mod corpse;
mod hero;
mod level;
mod physics;
mod score;
mod setup;
mod transform_link;
mod vfx;
mod weapons;

#[allow(unused)]
mod math_utils;

use bevy::{prelude::*, window::WindowResized};
use bevy_asepritesheet::core::AsepritesheetPlugin;
use bevy_poly_level::prelude::*;
use bevy_screen_diagnostics::*;

use bevy_xpbd_2d::{plugins::debug::PhysicsDebugConfig, prelude::PhysicsLayer};
use hero::*;
use setup::{PlayerCount, SetupSystemIds};

// -------------------------------------------------------------------------------------------------

#[derive(PhysicsLayer)]
pub enum PhysicsLayers {
    Terrain,
    Heros,
    FreshCorpses,
    StaleCorpses,
    Projectiles,
}

#[derive(Component, Debug, Clone, Copy)]
pub struct Remove;

// -------------------------------------------------------------------------------------------------

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(ImagePlugin::default_nearest()),
            ScreenDiagnosticsPlugin::default(),
            ScreenFrameDiagnosticsPlugin,
            ScreenEntityDiagnosticsPlugin,
            BevyLevelEditPlugin::new(true, false, Some(KeyCode::F5)),
            AsepritesheetPlugin::new(&["sprite.json"]),
        ))
        .add_plugins((
            controller::ControllerPlugin,
            physics::PhysicsPlugin,
            level::LevelPlugin,
            hero::HeroPlugin,
            weapons::WeaponsPlugin,
            transform_link::TransformLinkPlugin,
            setup::SetupPlugin,
            score::ScorePlugin,
            vfx::VfxPlugin,
            corpse::CorpsePlugin,
        ))
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (on_resize_window, handle_debug_stuff, handle_arena_reloading),
        )
        .run();
}

// -------------------------------------------------------------------------------------------------

fn setup(mut commands: Commands, sys_ids: Res<SetupSystemIds>) {
    commands.run_system(sys_ids.restart_arena);
}

fn handle_arena_reloading(
    mut commands: Commands,
    sys_ids: Res<SetupSystemIds>,
    mut plr_count: ResMut<PlayerCount>,
    key_in: Res<Input<KeyCode>>,
) {
    let mut reload = false;
    for key in key_in.get_just_pressed() {
        match key {
            KeyCode::Key1 => {
                plr_count.players = 1;
                reload = true;
            }
            KeyCode::Key2 => {
                plr_count.players = 2;
                reload = true;
            }
            KeyCode::Key3 => {
                plr_count.players = 3;
                reload = true;
            }
            KeyCode::Key4 => {
                plr_count.players = 4;
                reload = true;
            }
            KeyCode::Key5 => {
                plr_count.players = 5;
                reload = true;
            }
            KeyCode::Key6 => {
                plr_count.players = 6;
                reload = true;
            }
            KeyCode::Key7 => {
                plr_count.players = 7;
                reload = true;
            }
            KeyCode::Key8 => {
                plr_count.players = 8;
                reload = true;
            }
            KeyCode::Key9 => {
                plr_count.players = 9;
                reload = true;
            }
            _ => {}
        }
    }
    if reload {
        commands.run_system(sys_ids.restart_arena);
    }
}

fn on_resize_window(
    sys_ids: Res<SetupSystemIds>,
    mut commands: Commands,
    mut evts: EventReader<WindowResized>,
) {
    let mut resized = false;
    for _ in evts.read() {
        resized = true;
    }
    if resized {
        commands.run_system(sys_ids.resize_cam_vps);
    }
}

fn handle_debug_stuff(key_in: Res<Input<KeyCode>>, mut phys_rdr_debug: ResMut<PhysicsDebugConfig>) {
    if key_in.just_pressed(KeyCode::F2) {
        phys_rdr_debug.enabled = !phys_rdr_debug.enabled;
    }
}
