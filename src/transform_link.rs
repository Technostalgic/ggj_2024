use crate::math_utils::*;
use bevy::prelude::*;

// System Sets: ------------------------------------------------------------------------------------

#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct SyncTransformLinks;

// Sprite Link Plugin: -----------------------------------------------------------------------------

pub struct TransformLinkPlugin;

impl Plugin for TransformLinkPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(PostUpdate, SyncTransformLinks)
            .add_systems(PostUpdate, link_sprites.in_set(SyncTransformLinks));
    }
}

// Sprite Link Component: --------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct TransformLinker {
    pub linked_transform: Entity,
}

fn link_sprites(
    mut commands: Commands,
    mut link_query: Query<(Entity, &mut Transform, &TransformLinker)>,
    trans_query: Query<&GlobalTransform>,
) {
    for (ent, mut trans, linker) in &mut link_query {
        let Ok(target_trans) = trans_query.get(linker.linked_transform) else {
            commands.entity(ent).despawn();
            continue;
        };
        trans.translation = target_trans.translation();
        trans.set_rotation2d(target_trans.rotation2d());
    }
}

// General Relation Component: ---------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy, PartialEq, Eq)]
pub struct OwnedBy(pub Entity);
