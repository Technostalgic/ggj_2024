use bevy::{prelude::*, render::view::NoFrustumCulling};
use bevy_asepritesheet::{
    animator::{AnimatedSpriteBundle, SpriteAnimator},
    core::load_spritesheet_then,
    sprite::{AnimEndAction, AnimHandle, Spritesheet},
};
use bevy_xpbd_2d::components::LinearVelocity;
use rand::{thread_rng, Rng};

use crate::{math_utils::*, weapons::ProjectileCollision};

// Plugin: -----------------------------------------------------------------------------------------

pub struct VfxPlugin;

impl Plugin for VfxPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(VfxResource::default())
            .insert_resource(DefaultHitVfx(HitVfx::ParticleBurst(Color::RED)))
            .add_systems(Startup, load_vfx)
            .add_systems(Update, handle_vfx_anims)
            .add_systems(
                PostUpdate,
                (handle_projectile_hit_fx, handle_hit_fx, handle_particles).chain(),
            );
    }
}

// Resource: ---------------------------------------------------------------------------------------

#[derive(Resource)]
pub struct DefaultHitVfx(HitVfx);

#[derive(Resource, Default, Debug, Clone)]
pub struct VfxResource {
    pub small_spark: Handle<Spritesheet>,
}

fn load_vfx(
    mut commands: Commands,
    mut vfx_res: ResMut<VfxResource>,
    mut default_hit_fx: ResMut<DefaultHitVfx>,
    asset_server: Res<AssetServer>,
) {
    vfx_res.small_spark = load_spritesheet_then(
        &mut commands,
        &asset_server,
        "small_hit.sprite.json",
        bevy::sprite::Anchor::CenterRight,
        |sheet| {
            sheet
                .get_anim_mut(&AnimHandle::from_index(0))
                .unwrap()
                .end_action = AnimEndAction::Stop;
        },
    );
    default_hit_fx.0 = HitVfx::SpriteAnim(vfx_res.small_spark.clone());
}

// Vfx Component: ----------------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct VfxAnim;

fn handle_vfx_anims(
    mut commands: Commands,
    vfx_query: Query<(Entity, &SpriteAnimator), With<VfxAnim>>,
) {
    for (ent, anim) in &vfx_query {
        if anim.cur_anim().is_none() {
            commands.entity(ent).despawn();
        }
    }
}

// Hit Effect: -------------------------------------------------------------------------------------

#[derive(Component, Debug, Clone)]
pub enum HitVfx {
    SpriteAnim(Handle<Spritesheet>),
    ParticleBurst(Color),
}

fn handle_projectile_hit_fx(
    mut commands: Commands,
    mut proj_hits: EventReader<ProjectileCollision>,
    default_fx: Res<DefaultHitVfx>,
    hit_fx_query: Query<&HitVfx>,
) {
    for col in proj_hits.read() {
        for hit in &col.ordered_hits {
            let fx_type = hit_fx_query.get(hit.entity).map_or(&default_fx.0, |f| f);
            let trans = Transform::from_translation(hit.point1.extend(0.0));
            match fx_type {
                HitVfx::SpriteAnim(handle) => {
                    commands.spawn((
                        trans,
                        GlobalTransform::default(),
                        Vfx::SpriteAnim {
                            spritesheet: handle.clone(),
                            rotation: (-hit.normal1).to_angle(),
                        },
                    ));
                }
                HitVfx::ParticleBurst(color) => {
                    commands.spawn((
                        trans,
                        GlobalTransform::default(),
                        Vfx::ParticleBurst {
                            velocity: Vec2::ZERO,
                            burst_power: 1.0,
                            color: *color,
                        },
                    ));
                }
            }
        }
    }
}

// Effect: -----------------------------------------------------------------------------------------

#[derive(Component, Debug, Clone)]
pub enum Vfx {
    SpriteAnim {
        spritesheet: Handle<Spritesheet>,
        rotation: f32,
    },
    ParticleBurst {
        velocity: Vec2,
        burst_power: f32,
        color: Color,
    },
}

fn handle_hit_fx(mut commands: Commands, fx_query: Query<(Entity, &GlobalTransform, &Vfx)>) {
    for (ent, glob_trans, effect) in &fx_query {
        match effect {
            Vfx::SpriteAnim {
                spritesheet,
                rotation,
            } => {
                commands.spawn((
                    VfxAnim,
                    AnimatedSpriteBundle {
                        animator: SpriteAnimator::from_anim(AnimHandle::from_index(0)),
                        spritesheet: spritesheet.clone(),
                        sprite_bundle: SpriteSheetBundle {
                            transform: Transform {
                                translation: glob_trans.translation(),
                                rotation: Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, *rotation),
                                scale: Vec3::new(0.1, 0.1, 1.0),
                            },
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                ));
            }
            Vfx::ParticleBurst {
                velocity,
                burst_power: burst_speed,
                color,
            } => {
                particle_burst_at(
                    &mut commands,
                    glob_trans.translation().truncate(),
                    *velocity,
                    *burst_speed,
                    *color,
                );
            }
        }
        commands.entity(ent).despawn();
    }
}

fn particle_burst_at(
    commands: &mut Commands,
    position: Vec2,
    velocity: Vec2,
    power: f32,
    color: Color,
) {
    let p_count = 2 + power as u32;
    let speed = (4.0 + power * 0.5) * 5.0;
    for _ in 0..p_count {
        let vel = velocity
            + Vec2::from_angle(thread_rng().gen_range(0.0..TWO_PI))
                * thread_rng().gen_range(0.0..speed);
        commands.spawn((
            NoFrustumCulling,
            ParticleBundle {
                life: Lifetime::new(thread_rng().gen_range(0.4..0.65)),
                velocity: LinearVelocity(vel),
                sprite: SpriteBundle {
                    transform: Transform::from_translation(position.extend(1.0)),
                    sprite: Sprite {
                        color: color,
                        ..Default::default()
                    },
                    ..Default::default()
                },
                ..Default::default()
            },
        ));
    }
}

// Particles: --------------------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone)]
struct ParticleVfx;

#[derive(Bundle, Clone, Default)]
struct ParticleBundle {
    particle: ParticleVfx,
    life: Lifetime,
    velocity: LinearVelocity,
    sprite: SpriteBundle,
}

#[derive(Component, Debug, Clone)]
struct Lifetime {
    lifetime: f32,
    max_lifetime: f32,
}

impl Default for Lifetime {
    fn default() -> Self {
        Self {
            lifetime: 0.0,
            max_lifetime: 1.0,
        }
    }
}

impl Lifetime {
    pub fn new(max_life: f32) -> Self {
        Self {
            lifetime: 0.0,
            max_lifetime: max_life,
        }
    }
}

fn handle_particles(
    mut commands: Commands,
    time: Res<Time>,
    mut particle_query: Query<
        (
            Entity,
            &mut Transform,
            &mut LinearVelocity,
            &mut Sprite,
            &mut Lifetime,
        ),
        With<ParticleVfx>,
    >,
) {
    let dt = time.delta_seconds();
    let p_length = 0.1;
    let p_width = 0.1;
    let damping = 0.93 as f32;
    let damp_factor = damping.powf(dt * 60.0);
    for (ent, mut trans, mut vel, mut sprite, mut life) in &mut particle_query {
        if life.lifetime >= life.max_lifetime {
            commands.entity(ent).despawn();
            continue;
        }
        let life_delta = life.lifetime / life.max_lifetime;
        sprite.color = sprite.color.with_a(1.0 - life_delta);
        trans.translation += (vel.0 * dt).extend(0.0);
        trans.scale.y = p_width;
        trans.scale.x = (vel.0.length() * p_length).max(p_width);
        trans.set_rotation2d(vel.0.to_angle());
        vel.0 *= damp_factor;
        life.lifetime += dt;
    }
}
