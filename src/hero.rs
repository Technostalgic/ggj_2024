use std::collections::VecDeque;

use bevy::{input::InputSystem, prelude::*, render::view::NoFrustumCulling};
use bevy_asepritesheet::{
    animator::{AnimatedSpriteBundle, SpriteAnimator},
    core::load_spritesheet,
    sprite::{AnimHandle, Spritesheet},
};
use bevy_dynamic_gravity_2d::prelude::*;
use bevy_xpbd_2d::prelude::*;

use crate::{
    controller::{
        action::{Action, ActionReceiver},
        player::{PlayerId, PlayerInputSystem},
        ControllerSystem,
    },
    corpse::{CorpseAsset, CorpseEmitter},
    level::LevelSpawnPoints,
    math_utils::{IsZero, RotateVecQuarterAngle, Rotation2d},
    score::Scores,
    transform_link::TransformLinker,
    vfx::HitVfx,
    weapons::{LiveProjectile, ProjectileHandling},
    PhysicsLayers,
};

// Action Delegation System Set: -------------------------------------------------------------------

/// a system set that runs in preupdate after [`bevy::input::InputSystem`], a more
/// precise subset of [`ControllerSystem`] which only contains systems that are dealing directly
/// with input in a way directly related to delegating [`PlayerAction`]s to entities
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub(crate) struct ActionDelegationSystem;

#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct HealthChecking;

// Hero Plugin: ------------------------------------------------------------------------------------

pub struct HeroPlugin;

impl Plugin for HeroPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            PreUpdate,
            ActionDelegationSystem
                .after(InputSystem)
                .after(PlayerInputSystem),
        )
        .configure_sets(
            FixedUpdate,
            HealthChecking
                .after(ProjectileHandling)
                .before(PhysicsSet::Prepare),
        )
        .add_systems(
            PreUpdate,
            (parse_hero_actions, handle_aim_actions)
                .chain()
                .in_set(ActionDelegationSystem)
                .in_set(ControllerSystem),
        )
        .add_systems(
            FixedUpdate,
            (
                handle_health.in_set(HealthChecking),
                hero_actions.before(GroundMovement),
                handle_hero_respawn,
                handle_hit_target,
            ),
        )
        .add_systems(Update, (handle_hero_gfx, control_hero_anim));
    }
}

// Hero Colors: ------------------------------------------------------------------------------------

pub const HERO_COLORS: [Color; 8] = [
    Color::rgb(0.5, 0.5, 1.0),
    Color::rgb(1.0, 0.3, 0.3),
    Color::rgb(0.3, 1.0, 0.2),
    Color::rgb(0.9, 0.9, 0.1),
    Color::PINK,
    Color::PURPLE,
    Color::CYAN,
    Color::WHITE,
];

// Hero: -------------------------------------------------------------------------------------------

#[derive(Component, Clone, Copy)]
pub struct Hero {
    pub score: u32,
    movement: Vec2,
    is_interacting: bool,
}

impl Default for Hero {
    fn default() -> Self {
        Self {
            score: 0,
            is_interacting: default(),
            movement: default(),
        }
    }
}

fn parse_hero_actions(
    mut query: Query<(
        &ActionReceiver,
        &mut Hero,
        &mut GroundMover,
        &mut GroundJumper,
    )>,
) {
    for (act_rec, mut hero, mut mover, mut jumper) in &mut query {
        let mut is_jumping = false;
        for action in act_rec.recieved_actions() {
            match action {
                Action::MoveHorizontal(mov) => {
                    hero.movement.x += mov.0;
                }
                Action::MoveVertical(mov) => {
                    hero.movement.y += mov.0;
                }
                Action::Jump => {
                    is_jumping = true;
                }
                Action::Interact => {
                    hero.is_interacting = true;
                }
                _ => {}
            }
        }
        // signal other components for actions
        mover.set_absolute_movement(hero.movement);
        jumper.set_jumping(is_jumping);
    }
}

fn hero_actions(mut hero_query: Query<(&mut Hero, &Health)>) {
    for (mut hero, health) in &mut hero_query {
        if health.health <= 0.0 {
            continue;
        }
        if hero.is_interacting {
            warn!("implement interaction");
        }
        // reset action flags
        hero.movement.x = 0.0;
        hero.movement.y = 0.0;
        hero.is_interacting = false;
    }
}

// Hero Bundle: ------------------------------------------------------------------------------------

#[derive(Bundle)]
pub struct HeroBundle {
    pub spatial: SpatialBundle,
    pub hero: Hero,
    pub health: Health,
    pub respawn: Respawn,
    pub player_id: PlayerId,
    pub action_receiver: ActionReceiver,
    pub collider: Collider,
    pub orient: OrientToGravity,
    pub ground_jumper: GroundJumper,
    pub ground_mover: GroundMover,
    pub aim: Aim,
    pub hit_fx: HitVfx,
    pub collision_layers: CollisionLayers,
    pub corpse: Handle<CorpseAsset>,
    pub proj_target: ProjectileHitTarget,
    pub force: ExternalForce,
    pub impulse: ExternalImpulse,
    pub mass: Mass,
    grounded: Grounded,
    dynamic_gravity: DynamicGravity,
    rigid_body: RigidBody,
    pub gravity_scale: GravityScale,
    friction: Friction,
    restitution: Restitution,
    pub lin_vel: LinearVelocity,
    locked_axes: LockedAxes,
    sleep_disable: SleepingDisabled,
}

impl Default for HeroBundle {
    fn default() -> Self {
        let mut ground_mover = GroundMover::default();
        ground_mover.move_speed = 15.0;
        ground_mover.acceleration_factor = 3.0;
        Self {
            spatial: default(),
            hero: default(),
            health: Health::new(10.0),
            respawn: default(),
            player_id: default(),
            action_receiver: default(),
            collider: Collider::capsule(1.6, 0.75),
            orient: default(),
            grounded: default(),
            ground_mover,
            ground_jumper: default(),
            aim: default(),
            hit_fx: HitVfx::ParticleBurst(Color::RED),
            collision_layers: CollisionLayers::new(
                [PhysicsLayers::Heros],
                [
                    PhysicsLayers::Terrain,
                    PhysicsLayers::FreshCorpses,
                    PhysicsLayers::Projectiles,
                ],
            ),
            corpse: Default::default(),
            proj_target: ProjectileHitTarget::default(),
            force: ExternalForce::default().with_persistence(false),
            impulse: ExternalImpulse::default().with_persistence(false),
            mass: Mass(1.0),
            dynamic_gravity: default(),
            rigid_body: RigidBody::Dynamic,
            gravity_scale: GravityScale(3.0),
            friction: Friction::new(0.0),
            restitution: Restitution {
                coefficient: 0.0,
                combine_rule: CoefficientCombine::Multiply,
            },
            lin_vel: LinearVelocity::ZERO,
            locked_axes: LockedAxes::ROTATION_LOCKED,
            sleep_disable: SleepingDisabled,
        }
    }
}

// Projectile Target: ------------------------------------------------------------------------------

/// data structure for storing a projectile and it's hit data
#[derive(Debug, Clone)]
pub struct ProjectileHit(pub Entity, pub ShapeHitData, pub f32);

#[derive(Component, Debug, Clone)]
pub struct ProjectileHitTarget {
    /// the max amount of time in seconds that a projectile hit is stored for
    pub max_time: f32,
    hits: VecDeque<ProjectileHit>,
    cur_time: f32,
}

impl Default for ProjectileHitTarget {
    fn default() -> Self {
        Self {
            max_time: 0.1,
            cur_time: 0.0,
            hits: Default::default(),
        }
    }
}

impl ProjectileHitTarget {
    pub fn store_hit(&mut self, projectile: Entity, hit: ShapeHitData) {
        self.hits
            .push_back(ProjectileHit(projectile, hit, self.cur_time));
    }
    pub fn iter(&self) -> std::collections::vec_deque::Iter<'_, ProjectileHit> {
        self.hits.iter()
    }
}

fn handle_hit_target(time: Res<Time>, mut query: Query<&mut ProjectileHitTarget>) {
    let dt = time.delta_seconds();
    for mut targ in &mut query {
        targ.cur_time += dt;
        let min_time = targ.cur_time - targ.max_time;
        while let Some(hit) = targ.hits.front() {
            if hit.2 < min_time {
                targ.hits.pop_front();
            } else {
                break;
            }
        }
    }
}

// Health Component: -------------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct Health {
    pub max_health: f32,
    pub health: f32,
}

impl Default for Health {
    fn default() -> Self {
        Self {
            max_health: 10.0,
            health: 10.0,
        }
    }
}

impl Health {
    pub fn new(max_health: f32) -> Self {
        Self {
            max_health,
            health: max_health,
        }
    }
    pub fn heal_full(&mut self) {
        self.health = self.max_health;
    }
}

fn handle_health(
    mut commands: Commands,
    key_in: Res<Input<KeyCode>>,
    mut scores: ResMut<Scores>,
    mut health_query: Query<(
        Entity,
        &mut Health,
        Option<&mut Respawn>,
        Option<&Handle<CorpseAsset>>,
        Option<&ProjectileHitTarget>,
    )>,
    proj_query: Query<&PlayerId, LiveProjectile>,
) {
    for (ent, mut health, mut op_respawn, op_corpse, op_targ) in &mut health_query {
        if key_in.pressed(KeyCode::Delete) {
            // kill players on delete key
            health.health = 0.0;
            if let Some(respawn) = op_respawn.as_mut() {
                respawn.timer = 0.0;
            }
        }
        if health.health <= 0.0 {
            // die with respawn
            if let Some(mut respawn) = op_respawn {
                if respawn.is_respawning {
                    continue;
                } else {
                    // death with respawn
                    let mut ent_cmd = commands.entity(ent);
                    ent_cmd.insert(Sensor);
                    ent_cmd.remove::<DynamicGravity>();
                    respawn.is_respawning = true;
                }
            } else {
                // death without respawn
                commands.entity(ent).despawn();
            }
            health.health = 0.0;
            // add score for player who got the killing shot
            if let Some(target) = op_targ {
                for hit in target.iter() {
                    let Ok(pid) = proj_query.get(hit.0) else {
                        continue;
                    };
                    scores.add_score(pid.0, 1);
                    break;
                }
            }
            // spawn the corpse if available
            if let Some(corpse_handle) = op_corpse {
                corpse_handle.create_corpse_at(&mut commands, 0, ent)
            }
        }
    }
}

// Respawn: ----------------------------------------------------------------------------------------

#[derive(Component, Debug, Clone)]
pub struct Respawn {
    pub respawn_length: f32,
    pub timer: f32,
    is_respawning: bool,
}

impl Default for Respawn {
    fn default() -> Self {
        Self {
            timer: 0.0,
            respawn_length: 3.0,
            is_respawning: false,
        }
    }
}

fn handle_hero_respawn(
    mut commands: Commands,
    time: Res<Time>,
    spawn_points: Res<LevelSpawnPoints>,
    mut hero_query: Query<(Entity, &mut Transform, &mut Health, &mut Respawn)>,
) {
    let dt = time.delta_seconds();
    for (ent, mut trans, mut health, mut respawn) in &mut hero_query {
        if respawn.is_respawning {
            respawn.timer += dt;
            // respawn
            if respawn.timer >= respawn.respawn_length {
                respawn.timer = 0.0;
                respawn.is_respawning = false;
                let mut ent_cmd = commands.entity(ent);
                ent_cmd.remove::<Sensor>();
                ent_cmd.insert(DynamicGravity::default());
                health.heal_full();
                trans.translation = spawn_points.random().extend(trans.translation.z);
            }
        }
    }
}

// Aim Component: ----------------------------------------------------------------------------------

#[derive(Component, Default, Clone, Copy)]
pub struct Aim(pub Vec2);

pub(crate) fn handle_aim_actions(mut aim_query: Query<(&mut Aim, &ActionReceiver)>) {
    for (mut aim, actions) in &mut aim_query {
        for action in actions.recieved_actions() {
            match action {
                Action::AimHorizontal(x) => aim.0.x += x.0,
                Action::AimVertical(y) => aim.0.y += y.0,
                _ => {}
            }
        }
        if aim.0.length_squared() >= 1.0 {
            aim.0 = aim.0.normalize_or_zero();
        }
    }
}

// Hero Anim Controller: ---------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone)]
struct HeroAnimHandles {
    is_loaded: bool,
    handle_idle: AnimHandle,
    handle_run: AnimHandle,
    handle_jump: AnimHandle,
    handle_fall: AnimHandle,
}

fn control_hero_anim(
    spritesheet_assets: Res<Assets<Spritesheet>>,
    mut hero_gfx_query: Query<
        (
            &mut Transform,
            &Handle<Spritesheet>,
            &mut SpriteAnimator,
            &mut HeroAnimHandles,
            &TransformLinker,
        ),
        With<HeroGfxMarker>,
    >,
    hero_query: Query<(
        &GlobalTransform,
        &Grounded,
        &LinearVelocity,
        &DynamicGravity,
    )>,
) {
    for (mut trans, sheet_handle, mut animator, mut anims, linker) in &mut hero_gfx_query {
        // load handles if not loaded
        if !anims.is_loaded {
            if let Some(sheet) = spritesheet_assets.get(sheet_handle) {
                anims.is_loaded = true;
                anims.handle_idle = sheet.get_anim_handle("idle");
                anims.handle_run = sheet.get_anim_handle("run");
                anims.handle_jump = sheet.get_anim_handle("jump");
                anims.handle_fall = sheet.get_anim_handle("fall");
            } else {
                continue;
            }
        }
        // get hero physical components from hero query
        let Ok((glob_trans, grounded, vel, grav)) = hero_query.get(linker.linked_transform) else {
            continue;
        };
        // calculate relative hero up axis
        let up_axis = if grav.total_gravity().is_zero() {
            Vec2::from_angle(glob_trans.rotation2d()).rotate_ccw()
        } else {
            -grav.total_gravity().normalize()
        };
        let fwd_axis = up_axis.rotate_cw();
        let fwd_vel = fwd_axis.dot(vel.0);
        // handle running/idle anims while on ground
        if grounded.on_ground() {
            if fwd_vel.abs() > 0.5 {
                animator.set_anim(anims.handle_run);
            } else {
                animator.set_anim(anims.handle_idle);
            }
        // handle jump/fall anims while in air
        } else {
            let up_vel = up_axis.dot(vel.0);
            if up_vel > 0.0 {
                animator.set_anim(anims.handle_jump);
            } else {
                animator.set_anim(anims.handle_fall);
            }
        }
        // flip sprite based on forward velocity
        if fwd_vel > 0.1 {
            trans.scale.x = trans.scale.x.abs()
        } else if fwd_vel < -0.1 {
            trans.scale.x = -trans.scale.x.abs()
        }
    }
}

// Handle Hero Graphics: ---------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
struct HeroGfxMarker;

fn handle_hero_gfx(
    mut commands: Commands,
    new_heros: Query<(Entity, &PlayerId), Added<Hero>>,
    mut hero_gfxs: Query<(&mut Visibility, &TransformLinker), With<HeroGfxMarker>>,
    health_query: Query<&Health>,
    asset_server: Res<AssetServer>,
) {
    // iterate through each hero added this frame
    for (hero_ent, pid) in &new_heros {
        let sheet_handle = load_spritesheet(
            &mut commands,
            &asset_server,
            "hero.sprite.json",
            bevy::sprite::Anchor::Center,
        );
        commands.spawn((
            HeroGfxMarker,
            NoFrustumCulling,
            HeroAnimHandles::default(),
            TransformLinker {
                linked_transform: hero_ent,
            },
            AnimatedSpriteBundle {
                spritesheet: sheet_handle,
                animator: SpriteAnimator::new(1.0),
                sprite_bundle: SpriteSheetBundle {
                    transform: Transform::from_scale(Vec3::new(0.1, 0.1, 1.0)),
                    sprite: TextureAtlasSprite {
                        color: HERO_COLORS[pid.0],
                        ..Default::default()
                    },
                    ..Default::default()
                },
                ..Default::default()
            },
        ));
    }
    // iterate through heros and update theier sprites to alive
    for (mut vis, trans_link) in &mut hero_gfxs {
        if let Ok(health) = health_query.get(trans_link.linked_transform) {
            if health.health > 0.0 {
                *vis = Visibility::Visible;
            } else {
                *vis = Visibility::Hidden;
            }
        }
    }
}
