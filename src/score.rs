use bevy::{core_pipeline::clear_color::ClearColorConfig, prelude::*};

use crate::{setup::PlayerCount, HERO_COLORS};

// Score Plugin: -----------------------------------------------------------------------------------

pub struct ScorePlugin;

impl Plugin for ScorePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Scores::default())
            .add_systems(Startup, setup)
            .add_systems(PostUpdate, update_ui);
    }
}

// Scores: -----------------------------------------------------------------------------------------

#[derive(Resource, Default, Clone)]
pub struct Scores {
    scores: Vec<u32>,
}

impl Scores {
    pub fn get_score(&self, player_id: usize) -> u32 {
        if player_id >= self.scores.len() {
            0
        } else {
            self.scores[player_id]
        }
    }
    pub fn add_score(&mut self, player_id: usize, amount: u32) {
        while player_id >= self.scores.len() {
            self.scores.push(0);
        }
        self.scores[player_id] += amount;
    }
    pub fn reset_scores(&mut self) {
        self.scores.clear();
    }
}

// Score Ui: ---------------------------------------------------------------------------------------

#[derive(Component)]
pub struct ScoreUi;

#[derive(Component)]
pub struct UiCamera;

fn setup(mut commands: Commands) {
    commands.spawn((
        UiCamera,
        UiCameraConfig { show_ui: true },
        Camera2dBundle {
            camera: Camera {
                order: 255,
                ..Default::default()
            },
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::None,
                ..Default::default()
            },
            projection: OrthographicProjection {
                near: 100.0,
                ..Default::default()
            },
            ..Default::default()
        },
    ));
    commands.spawn((
        ScoreUi,
        Text2dBundle {
            transform: Transform::from_translation(Vec3::new(0.0, 0.0, -101.0)),
            text: Text {
                sections: [TextSection {
                    style: TextStyle {
                        font_size: 100.0,
                        ..Default::default()
                    },
                    value: "GAME".to_string(),
                }]
                .to_vec(),
                ..Default::default()
            },
            ..Default::default()
        },
    ));
}

fn update_ui(
    scores: Res<Scores>,
    plr_count: Res<PlayerCount>,
    ui_cam_query: Query<(&GlobalTransform, &Camera), With<UiCamera>>,
    mut ui_query: Query<(&mut Transform, &mut Text), With<ScoreUi>>,
) {
    let (ui_cam_glob_trans, ui_cam) = ui_cam_query.single();
    let (mut ui_trans, mut ui_text) = ui_query.single_mut();
    let plrs = plr_count.players;
    if plrs < 1 {
        return;
    }
    if plrs as usize != ui_text.sections.len() {
        ui_text.sections.clear();
        for i in 0..plrs {
            ui_text.sections.push(TextSection {
                style: TextStyle {
                    color: HERO_COLORS[i as usize],
                    font_size: 50.0,
                    ..Default::default()
                },
                value: "0 ".to_string(),
            });
        }
    } else {
        for i in 0..plrs {
            let sec = &mut ui_text.sections[i as usize];
            sec.value = format!("{} ", scores.get_score(i as usize));
        }
    }
    let pos = ui_cam
        .ndc_to_world(ui_cam_glob_trans, Vec3::new(0.0, -1.0, 0.0))
        .unwrap();
    ui_trans.translation.x = pos.x;
    ui_trans.translation.y = pos.y + 30.0;
}
