use bevy::{prelude::*, render::view::NoFrustumCulling};
use bevy_dynamic_gravity_2d::core::math_utils::TransformPoint2D;
use bevy_xpbd_2d::prelude::*;

use crate::{
    controller::player::{PlayerId, PLAYER_NONE},
    math_utils::{MutRotation2d, ToAngle},
    transform_link::{OwnedBy, SyncTransformLinks, TransformLinker},
    Health, PhysicsLayers, Remove, HERO_COLORS,
};

use super::{
    LiveProjectile, Projectile, ProjectileCollision, ProjectileHandling, ReflectAcross,
    StandardProjCollision, Weapon,
};

// Projectile Plugin: ------------------------------------------------------------------------------

pub struct BasicProjectilePlugin;

impl Plugin for BasicProjectilePlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            FixedUpdate,
            (validate_proj, handle_proj_hit, proj_lifetime)
                .chain()
                .in_set(ProjectileHandling),
        )
        .add_systems(PostUpdate, handle_proj_graphics.after(SyncTransformLinks));
    }
}

// Projectile Stat Components: ---------------------------------------------------------------------

#[derive(Component, Debug, Default, Clone)]
pub struct Lifetime {
    /// the current age of the projectile; how long it has lived for
    pub lifetime: f32,
    /// the max lifetime of the projectile; if it exceeds this value it will be destroyed
    pub max_lifetime: f32,
}

impl Lifetime {
    pub fn new(max_life: f32) -> Self {
        Self {
            max_lifetime: max_life,
            ..Default::default()
        }
    }
}

#[derive(Component, Debug, Default, Clone)]
pub struct Damage(pub f32);

#[derive(Component, Debug, Default, Clone)]
pub struct Knockback(pub f32);

#[derive(Component, Debug, Default, Clone)]
pub struct Bounce {
    /// the dot product threshold between the normalized projectile velocity and the collision
    /// normal that must be surpassed in order for the bounce to occur. 0 to 1
    pub bounce_threshold: f32,
    /// the speed threshold that must be surpassed otherwise the projectile will not bounce
    pub min_speed: f32,
    /// the projectile velocity is multiplied by this value after each bounce
    pub bounce_damping: f32,
    /// the maximum amount of times the projectile can bounce
    pub max_bounces: u32,
    /// the amount of times the projectile has bounced
    pub bounces: u32,
}

// Projectile Systems: -----------------------------------------------------------------------------

fn validate_proj(
    mut commands: Commands,
    proj_query: Query<(Entity, &OwnedBy), (LiveProjectile, Without<PlayerId>)>,
    wep_query: Query<&PlayerId, With<Weapon>>,
) {
    for (ent, wep_owner) in &proj_query {
        let mut ent_cmd = commands.entity(ent);
        let pid = if let Ok(pid) = wep_query.get(wep_owner.0) {
            pid.0
        } else {
            PLAYER_NONE
        };
        ent_cmd.insert(PlayerId(pid));
    }
}

fn handle_proj_hit(
    mut commands: Commands,
    mut col_events: EventReader<ProjectileCollision>,
    mut proj_query: Query<
        (
            Entity,
            &mut Transform,
            &mut LinearVelocity,
            Option<&Damage>,
            Option<&Knockback>,
            Option<&mut Bounce>,
            Option<&mut StandardProjCollision>,
        ),
        LiveProjectile,
    >,
    mut potential_hit_query: Query<
        (
            &GlobalTransform,
            Option<&mut Health>,
            Option<&mut ExternalImpulse>,
            Option<&CenterOfMass>,
        ),
        Without<Projectile>,
    >,
) {
    for evt in col_events.read() {
        for hit in evt.ordered_hits.iter() {
            let Ok((proj_ent, mut proj_trans, mut vel, op_dmg, op_knockback, op_bounce, op_col)) =
                proj_query.get_mut(evt.projectile)
            else {
                continue;
            };
            let proj_dmg = op_dmg.map_or(0.0, |d| d.0);
            let proj_knockback = op_knockback.map_or(0.0, |k| k.0);
            if let Ok((hit_trans, op_health, op_impulse, op_cent_mass)) =
                potential_hit_query.get_mut(hit.entity)
            {
                let mut remove = true;
                // apply knockback
                if let Some(mut impulse) = op_impulse {
                    let center_of_mass = op_cent_mass.map_or(Vec2::ZERO, |c| c.0);
                    let force = vel.normalize_or_zero() * proj_knockback;
                    impulse.apply_impulse_at_point(
                        force,
                        hit_trans.inverse_transform_point2(hit.point1),
                        center_of_mass,
                    );
                }
                // bounce if applicable
                if let Some(mut bounce) = op_bounce {
                    if bounce.bounces >= bounce.max_bounces
                        || vel.0.normalize_or_zero().dot(-hit.normal1) > bounce.bounce_threshold
                        || vel.0.length_squared() < bounce.min_speed * bounce.min_speed
                    {
                        remove = true;
                    } else {
                        proj_trans.translation.x = hit.point1.x;
                        proj_trans.translation.y = hit.point1.y;
                        vel.0 = vel.0.reflect_across(hit.normal1) * bounce.bounce_damping;
                        bounce.bounces += 1;
                        remove = false;
                        if let Some(mut col) = op_col {
                            col.last_position = hit.point1;
                        }
                    }
                }
                // apply damage
                if let Some(mut health) = op_health {
                    health.health -= proj_dmg;
                    remove = true;
                }
                // flag the projectile for removal
                if remove {
                    commands.entity(proj_ent).insert(Remove);
                }
            }
            // we only want to handle the closest hit
            break;
        }
    }
}

fn proj_lifetime(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &mut Lifetime), LiveProjectile>,
) {
    let dt = time.delta_seconds();
    for (ent, mut life) in &mut query {
        life.lifetime += dt;
        if life.lifetime >= life.max_lifetime {
            commands.entity(ent).insert(Remove);
        }
    }
}

// Projectile Bundle: ------------------------------------------------------------------------------

#[derive(Bundle, Clone)]
pub struct BasicProjBundle {
    pub projectile: Projectile,
    pub col_method: StandardProjCollision,
    pub col_layers: CollisionLayers,
    pub collider: Collider,
    pub rigid_body: RigidBody,
    pub transform: Transform,
    pub glob_trans: GlobalTransform,
    pub lin_vel: LinearVelocity,
    pub sense_col: Sensor,
    pub owned_by: OwnedBy,
    pub player_id: PlayerId,
    pub life: Lifetime,
}

impl Default for BasicProjBundle {
    fn default() -> Self {
        Self {
            projectile: Projectile::default(),
            col_method: StandardProjCollision::default(),
            col_layers: CollisionLayers::new(
                [PhysicsLayers::Projectiles],
                [
                    PhysicsLayers::Projectiles,
                    PhysicsLayers::Terrain,
                    PhysicsLayers::Heros,
                    PhysicsLayers::FreshCorpses,
                ],
            ),
            collider: Collider::ball(0.05),
            rigid_body: RigidBody::Dynamic,
            transform: Default::default(),
            glob_trans: Default::default(),
            lin_vel: Default::default(),
            sense_col: Sensor,
            owned_by: OwnedBy(Entity::PLACEHOLDER),
            player_id: PlayerId(PLAYER_NONE),
            life: Lifetime::new(4.0),
        }
    }
}

// Projectile Graphics: ----------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone)]
pub struct BasicProjectileGfx;

fn handle_proj_graphics(
    mut commands: Commands,
    new_projs: Query<
        (Entity, &LinearVelocity, &Collider, &PlayerId),
        (Added<BasicProjectileGfx>, LiveProjectile),
    >,
    mut proj_gfxs: Query<
        (Entity, &mut Sprite, &mut Transform, &TransformLinker),
        Without<Projectile>,
    >,
    mut projs: Query<(&mut Transform, &LinearVelocity, &PlayerId), LiveProjectile>,
) {
    for (ent, vel, col, pid) in &new_projs {
        let aabb = col.compute_aabb(Default::default(), 0.0);
        let width = aabb.0.maxs.x - aabb.0.mins.x;
        let height = aabb.0.maxs.y - aabb.0.mins.y;
        let mut trans = Transform::from_scale(Vec3::new(width, height, 1.0));
        let dir = vel.0.to_angle();
        let norm_vel = vel.0.normalize_or_zero();
        trans.set_rotation2d(dir);
        trans.translation.x -= norm_vel.x * width;
        trans.translation.y -= norm_vel.y * width;
        commands.spawn((
            BasicProjectileGfx,
            NoFrustumCulling,
            TransformLinker {
                linked_transform: ent,
            },
            SpriteBundle {
                transform: trans,
                sprite: Sprite {
                    color: HERO_COLORS[pid.0],
                    ..Default::default()
                },
                ..Default::default()
            },
        ));
    }
    for (proj_gfx_ent, mut sprt, mut trans, link) in &mut proj_gfxs {
        if let Ok((mut proj_trans, vel, pid)) = projs.get_mut(link.linked_transform) {
            let dir = vel.0.to_angle();
            let norm_vel = vel.0.normalize_or_zero();
            proj_trans.set_rotation2d(dir);
            let width = trans.scale.y.max(vel.0.length() * 0.0175);
            trans.scale.x = width;
            trans.translation.x -= norm_vel.x * width;
            trans.translation.y -= norm_vel.y * width;
            sprt.color = HERO_COLORS[pid.0];
        } else {
            commands.entity(proj_gfx_ent).despawn();
        }
    }
}
