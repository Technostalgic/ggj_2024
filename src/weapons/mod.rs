pub mod basic_gun;
pub mod basic_projectile;

use crate::controller::player::PlayerId;
use crate::controller::player::PLAYER_NONE;
use crate::handle_aim_actions;
use crate::math_utils::*;
use crate::transform_link::OwnedBy;
use crate::Health;
use crate::ProjectileHitTarget;
use crate::Remove;
use crate::HERO_COLORS;
use bevy::prelude::*;
use bevy::render::view::NoFrustumCulling;
use bevy_xpbd_2d::prelude::*;

use crate::ActionDelegationSystem;
use crate::Aim;

use self::basic_gun::BasicGun;
use self::basic_gun::BasicGunStats;

// System Sets: ------------------------------------------------------------------------------------

#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct WeaponHandling;

#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct WeaponShooting;

#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct ProjectileHandling;

/// system set where all the projectile collision checks occur
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct ProjectileCollisionChecking;

// Plugin: -----------------------------------------------------------------------------------------

pub struct WeaponsPlugin;

impl Plugin for WeaponsPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ProjectileCollision>()
            .add_event::<WeaponShot>()
            .configure_sets(
                FixedUpdate,
                WeaponHandling
                    .after(ProjectileCollisionChecking)
                    .after(ProjectileHandling)
                    .before(PhysicsSet::Prepare),
            )
            .configure_sets(
                FixedUpdate,
                ProjectileCollisionChecking.before(PhysicsSet::Prepare),
            )
            .configure_sets(
                FixedUpdate,
                ProjectileHandling
                    .after(ProjectileCollisionChecking)
                    .before(PhysicsSet::Prepare),
            )
            .add_plugins((
                basic_projectile::BasicProjectilePlugin,
                basic_gun::BasicGunPlugin,
            ))
            .add_systems(
                PreUpdate,
                handle_equip_weapon
                    .after(handle_aim_actions)
                    .in_set(ActionDelegationSystem),
            )
            .add_systems(
                FixedUpdate,
                (
                    preprocess_projectile.before(ProjectileHandling),
                    (validate_standard_proj_col, standard_proj_collision_check)
                        .chain()
                        .in_set(ProjectileCollisionChecking),
                    handle_proj_evt_target_hits.in_set(ProjectileHandling),
                ),
            )
            .add_systems(
                Update,
                (handle_weapon_cursors, handle_cursor_reload_anim).chain(),
            );
    }
}

// Projectile: -------------------------------------------------------------------------------------

/// Component that should be used on any entity which behaves as a projectile
#[derive(Component, Default, Debug, Clone)]
pub struct Projectile;

/// WOrld query filter for projectile entities that aren't flagged for removal
pub type LiveProjectile = (With<Projectile>, Without<Remove>);

/// despawn any projectiles flagged for it
fn preprocess_projectile(
    mut commands: Commands,
    removed_proj_query: Query<Entity, (With<Projectile>, With<Remove>)>,
) {
    for ent in &removed_proj_query {
        commands.entity(ent).despawn();
    }
}

// Projectile Collision: ---------------------------------------------------------------------------

#[derive(Event, Debug, Clone)]
pub struct ProjectileCollision {
    pub projectile: Entity,
    pub ordered_hits: Vec<ShapeHitData>,
}

#[derive(Component, Debug, Clone)]
pub struct StandardProjCollision {
    last_position: Vec2,
    /// the max amount of shapecast hits that can occur for each collision check step, this value
    /// should usually be pretty low, unless the projectile will potentially be passing through
    /// multiple solid objects
    pub max_hits: u32,
}

impl Default for StandardProjCollision {
    fn default() -> Self {
        Self {
            last_position: Default::default(),
            max_hits: 2,
        }
    }
}

/// validate all standard projectile collisions on the first step they are added to the world
fn validate_standard_proj_col(
    mut new_projs: Query<
        (&GlobalTransform, &mut StandardProjCollision),
        Added<StandardProjCollision>,
    >,
) {
    for (glob_trans, mut std_col) in &mut new_projs {
        std_col.last_position = glob_trans.translation().truncate();
    }
}

/// the system which performs collision checks for all [`StandardProjCollision`] projectiles
fn standard_proj_collision_check(
    mut events: EventWriter<ProjectileCollision>,
    spatial: SpatialQuery,
    mut proj_query: Query<
        (
            Entity,
            &GlobalTransform,
            &mut StandardProjCollision,
            &Collider,
            Option<&PlayerId>,
            Option<&CollisionLayers>,
        ),
        LiveProjectile,
    >,
    hit_query: Query<(Option<&PlayerId>, Option<&Sensor>), With<Collider>>,
) {
    for (ent, glob_trans, mut std_col, collider, maybe_pid, maybe_col_layers) in &mut proj_query {
        let target = glob_trans.translation().truncate();
        let dif = target - std_col.last_position;
        if dif.is_zero() {
            continue;
        }
        // get the collision mask if available or just use an all inclusive mask (u32::MAX)
        let col_mask = maybe_col_layers.map_or(u32::MAX, |l| l.masks_bits());
        // query hits against the projectile's path
        let mut hits = spatial.shape_hits(
            collider,
            std_col.last_position,
            0.0,
            dif.normalize(),
            dif.length(),
            std_col.max_hits,
            true,
            SpatialQueryFilter::new()
                .without_entities([ent])
                .with_masks_from_bits(col_mask),
        );
        // filter out hits that we don't want
        let pid = maybe_pid.map_or(PLAYER_NONE, |p| p.0);
        for i in (0..hits.len()).rev() {
            let hit = hits[i];
            let Ok((maybe_o_pid, maybe_o_sensor)) = hit_query.get(hit.entity) else {
                continue;
            };
            // filter out sensor colliders and objects that are part of the same player
            let cut = maybe_o_sensor.is_some()
                || if pid != PLAYER_NONE && maybe_o_pid.is_some() {
                    pid == maybe_o_pid.unwrap().0
                } else {
                    false
                };
            if cut {
                hits.remove(i);
            }
        }
        if hits.len() > 0 {
            events.send(ProjectileCollision {
                projectile: ent,
                ordered_hits: hits,
            });
        }
        std_col.last_position = target;
    }
}

// Projectile Handling: ----------------------------------------------------------------------------

/// store all projectile hits in the targets they hit
fn handle_proj_evt_target_hits(
    mut events: EventReader<ProjectileCollision>,
    mut hit_query: Query<&mut ProjectileHitTarget>,
) {
    for evt in events.read() {
        for hit in &evt.ordered_hits {
            if let Ok(mut target) = hit_query.get_mut(hit.entity) {
                target.store_hit(evt.projectile, hit.clone());
            }
        }
    }
}

// Weapon: -----------------------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone, Copy)]
pub struct Weapon;

#[derive(Component, Debug, Clone, Copy)]
pub struct EquippedWeapon {
    pub weapon: Entity,
    pub local_offset: Vec2,
    pub arm_length: f32,
}

impl EquippedWeapon {
    /// create a new equipped weapon component that points to the specified weapon, with the
    /// specified local offset from the parent entity, and the arm length, which controls how far
    /// it moves from the origin based on aim
    pub fn new(weapon: Entity, offset: Vec2, arm_length: f32) -> Self {
        Self {
            weapon,
            local_offset: offset,
            arm_length,
        }
    }
    pub fn _from_ent(wep_entity: Entity) -> Self {
        Self {
            weapon: wep_entity,
            local_offset: Vec2::ZERO,
            arm_length: 0.0,
        }
    }
}

fn handle_equip_weapon(
    equipper_query: Query<(&GlobalTransform, &EquippedWeapon, Option<&Aim>)>,
    mut wep_query: Query<&mut Transform, With<Weapon>>,
) {
    for (glob_trans, equipped, maybe_aim) in &equipper_query {
        if let Ok(mut wep_trans) = wep_query.get_mut(equipped.weapon) {
            let mut pos = glob_trans.transform_point2(equipped.local_offset);
            let mut dir = glob_trans.rotation2d();
            if let Some(aim) = maybe_aim {
                pos += aim.0 * equipped.arm_length;
                dir = aim.0.to_angle();
            }
            wep_trans.translation = pos.extend(glob_trans.translation().z);
            wep_trans.set_rotation2d(dir);
        }
    }
}

// Weapon Shot: ------------------------------------------------------------------------------------

#[derive(Event, Debug, Clone)]
pub struct WeaponShot {
    pub weapon: Entity,
    pub projectiles: Vec<Entity>,
}

// Handle Weapon Cursor: ---------------------------------------------------------------------------

const WEAPON_RELOAD_ANIM_SEGS: usize = 8;

#[derive(Component, Debug, Clone)]
pub struct WeaponCursorGfx {
    equipper_ent: Entity,
}

#[derive(Component, Debug, Clone)]
struct WeaponCursorReloadGfx(usize);

fn handle_weapon_cursors(
    mut commands: Commands,
    mut cursor_query: Query<(
        Entity,
        &mut Transform,
        &mut Visibility,
        &mut Sprite,
        &mut WeaponCursorGfx,
    )>,
    aimer_query: Query<(&Health, &GlobalTransform, &Aim, Option<&EquippedWeapon>)>,
    new_weapon_equips: Query<(Entity, &PlayerId, &EquippedWeapon), Added<EquippedWeapon>>,
    wep_query: Query<&BasicGun>,
) {
    for (ent, pid, eq_wep) in &new_weapon_equips {
        // todo spawn cursor
        let col = HERO_COLORS[pid.0];
        let col = Color::rgb(col.r() * 0.5, col.g() * 0.5, col.b() * 0.5);
        let gfx_ent = commands
            .spawn((
                NoFrustumCulling,
                WeaponCursorGfx { equipper_ent: ent },
                SpriteBundle {
                    transform: Transform::from_scale(Vec3::new(1.0, 0.2, 1.0)),
                    sprite: Sprite {
                        color: col,
                        ..Default::default()
                    },
                    ..Default::default()
                },
            ))
            .id();
        if wep_query.get(eq_wep.weapon).is_ok() {
            for i in 0..WEAPON_RELOAD_ANIM_SEGS {
                let ang = (i as f32) / (WEAPON_RELOAD_ANIM_SEGS as f32) * TWO_PI;
                let trans = Transform {
                    translation: Vec3::new(0.0, 0.0, 0.0),
                    rotation: Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, ang),
                    scale: Vec3::new(0.1, 0.1, 1.0),
                    ..Default::default()
                };
                commands.spawn((
                    NoFrustumCulling,
                    WeaponCursorReloadGfx(i),
                    OwnedBy(gfx_ent),
                    SpriteBundle {
                        transform: trans,
                        ..Default::default()
                    },
                ));
            }
        }
    }
    for (cursor_ent, mut cursor_trans, mut vis, mut sprite, cursor) in &mut cursor_query {
        if let Ok((health, hero_trans, aim, maybe_eq_wep)) = aimer_query.get(cursor.equipper_ent) {
            let Some(eq_wep) = maybe_eq_wep else {
                commands.entity(cursor_ent).despawn();
                continue;
            };
            if let Ok(wep) = wep_query.get(eq_wep.weapon) {
                if wep.reload_wait() > 0.0 {
                    sprite.color = sprite.color.with_a(0.0);
                } else {
                    sprite.color = sprite.color.with_a(1.0);
                }
            }
            let off = aim.0 * 5.0;
            cursor_trans.translation =
                (hero_trans.transform_point2(eq_wep.local_offset) + off).extend(0.0) + Vec3::Z;
            cursor_trans.set_rotation2d(off.to_angle());
            if health.health > 0.0 {
                *vis = Visibility::Visible;
            } else {
                *vis = Visibility::Hidden;
            }
        } else {
            commands.entity(cursor_ent).despawn();
        }
    }
}

fn handle_cursor_reload_anim(
    mut commands: Commands,
    mut anim_seg_query: Query<
        (
            Entity,
            &mut Transform,
            &mut Visibility,
            &mut Sprite,
            &OwnedBy,
            &WeaponCursorReloadGfx,
        ),
        Without<WeaponCursorGfx>,
    >,
    cursor_query: Query<(&GlobalTransform, &Visibility, &Sprite, &WeaponCursorGfx)>,
    wep_equips_query: Query<&EquippedWeapon>,
    wep_query: Query<(&BasicGun, &BasicGunStats)>,
) {
    let alpha0 = 0.25;
    let alpha1 = 1.0;
    let scale0 = 0.2;
    let scale1 = 0.3;
    let anim_radius = 0.85;
    for (ent, mut trans, mut vis, mut sprite, owner, seg) in &mut anim_seg_query {
        // if the cursor does not exist, despawn segment and skip
        let Ok((cursor_trans, cursor_vis, cursor_sprite, cursor)) = cursor_query.get(owner.0)
        else {
            commands.entity(ent).despawn();
            continue;
        };
        // if the cursor is not visible, hide segment and skip
        if cursor_vis != Visibility::Visible {
            *vis = Visibility::Hidden;
            continue;
        }
        // skip if no weapon coresponding to the segment, no need to despawn because that the cursor
        // will despawn, which will cause the segment to despawn in the previous check
        let (wep, wep_stats) = match wep_equips_query.get(cursor.equipper_ent) {
            Ok(wep_eq) => match wep_query.get(wep_eq.weapon) {
                Ok(wep) => wep,
                _ => continue,
            },
            _ => continue,
        };
        // hide the segments if fully reloaded
        let reload_perc = wep.reload_wait() / wep_stats.reload_time;
        if reload_perc <= 0.0 {
            *vis = Visibility::Hidden;
            continue;
        }
        *vis = Visibility::Visible;
        let ang_perc = (seg.0 as f32) / (WEAPON_RELOAD_ANIM_SEGS as f32);
        let ang = ang_perc * TWO_PI + HALF_PI;
        let off = Vec2::from_angle(ang) * anim_radius;
        trans.translation = cursor_trans.translation() + off.extend(0.0);
        if ang_perc >= reload_perc {
            sprite.color = cursor_sprite.color.with_a(alpha1);
            trans.scale.x = scale1;
            trans.scale.y = scale1;
        } else {
            sprite.color = cursor_sprite.color.with_a(alpha0);
            trans.scale.x = scale0;
            trans.scale.y = scale0;
        }
    }
}
