use bevy::prelude::*;
use bevy_dynamic_gravity_2d::core::physics::dynamic_gravity::DynamicGravity;
use bevy_xpbd_2d::components::{Collider, GravityScale, LinearDamping, LinearVelocity};
use rand::{thread_rng, Rng};

use crate::{
    controller::{
        action::{Action, ActionReceiver},
        player::PlayerId,
    }, math_utils::{MutRotation2d, Rotation2d}, transform_link::OwnedBy, Health, Hero
};

use super::{
    basic_projectile::{BasicProjBundle, BasicProjectileGfx, Bounce, Damage, Knockback, Lifetime},
    Weapon, WeaponHandling, WeaponShooting, WeaponShot,
};

// Plugin: -----------------------------------------------------------------------------------------

pub struct BasicGunPlugin;

impl Plugin for BasicGunPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            FixedUpdate,
            (
                (
                    validate_basic_guns,
                    handle_basic_gun_actions,
                    handle_basic_guns,
                    shoot_basic_guns,
                )
                    .chain()
                    .in_set(WeaponHandling),
                handle_gun_shot.in_set(WeaponShooting),
            ),
        );
    }
}

// Basic Gun Bundle: -------------------------------------------------------------------------------

#[derive(Bundle, Default, Debug, Clone)]
pub struct BasicGunBundle {
    pub weapon: Weapon,
    pub gun: BasicGun,
    pub stats: BasicGunStats,
    pub transform: Transform,
    pub glob_trans: GlobalTransform,
    pub player_id: PlayerId,
    pub gfx: BasicProjectileGfx,
}

// Basic Gun: --------------------------------------------------------------------------------------

#[derive(Component, Default, Debug, Clone)]
pub struct BasicGun {
    is_triggered: bool,
    was_triggered: bool,
    is_reload_triggered: bool,
    fire_wait: f32,
    reload_wait: f32,
    rounds: u32,
}

impl BasicGun {
    /// start reloading the weapon
    fn reload(&mut self, stats: &BasicGunStats) {
        self.reload_wait = stats.reload_time;
        self.fire_wait = 0.0;
    }
    // reset/step the action flag fields in the gun struct
    fn step_action_state(&mut self) {
        self.was_triggered = self.is_triggered;
        self.is_triggered = false;
        self.is_reload_triggered = false;
    }
    pub fn reload_wait(&self) -> f32 {
        self.reload_wait
    }
}

fn validate_basic_guns(
    mut commands: Commands,
    gun_query: Query<(Entity, &PlayerId), Added<BasicGun>>,
    hero_query: Query<(Entity, &PlayerId), With<Hero>>,
) {
    for (ent, p_id) in &gun_query {
        let mut ent_cmd = commands.entity(ent);
        // link to hero entity with corresponding player id
        for (hero_ent, hero_id) in &hero_query {
            if hero_id == p_id {
                ent_cmd.insert(OwnedBy(hero_ent));
                break;
            }
        }
        // add action receiver component
        ent_cmd.insert(ActionReceiver::default());
    }
}

fn handle_basic_gun_actions(mut gun_query: Query<(&mut BasicGun, &ActionReceiver)>) {
    for (mut gun, actions) in &mut gun_query {
        gun.step_action_state();
        for action in actions.recieved_actions() {
            match action {
                Action::Attack => {
                    gun.is_triggered = true;
                }
                Action::Reload => {
                    gun.is_reload_triggered = true;
                }
                _ => {}
            }
        }
    }
}

fn handle_basic_guns(time: Res<Time>, mut gun_query: Query<(&mut BasicGun, &BasicGunStats)>) {
    for (mut gun, stats) in &mut gun_query {
        let dt = time.delta_seconds();
        // if gun is reloading
        if gun.reload_wait > 0.0 {
            gun.reload_wait -= dt;
            if gun.reload_wait <= 0.0 {
                gun.reload_wait = 0.0;
                gun.rounds = stats.clip_rounds;
            }
        // if gun is cooling gown
        } else if gun.fire_wait > 0.0 {
            gun.fire_wait -= dt;
            if gun.fire_wait <= 0.0 {
                gun.fire_wait = 0.0;
            }
        // gun is ready to fire
        } else {
            if gun.is_reload_triggered || gun.rounds <= 0 {
                gun.reload(stats);
            }
        }
    }
}

fn shoot_basic_guns(
    mut commands: Commands,
    mut gun_query: Query<(
        Entity,
        &GlobalTransform,
        &mut BasicGun,
        &BasicGunStats,
        &OwnedBy,
        &PlayerId,
    )>,
    mut owner_vel_query: Query<&mut LinearVelocity>,
    owner_health_query: Query<&Health>,
    mut events: EventWriter<WeaponShot>,
) {
    for (ent, glob_trans, mut gun, stats, owner, pid) in &mut gun_query {
        if let Ok(owner_health) = owner_health_query.get(owner.0) {
            if owner_health.health <= 0.0 {
                continue;
            }
        }
        // determine if gun can fire
        let mut can_fire = gun.rounds > 0 && gun.reload_wait <= 0.0 && gun.fire_wait <= 0.0;
        if stats.semi_automatic && gun.was_triggered {
            can_fire = false;
        }
        // stop here if gun cannot fire or is not triggered
        if !can_fire || !gun.is_triggered {
            continue;
        }
        // fire the projectiles from the weapon
        let proj_pos = glob_trans.translation().truncate();
        let proj_dir = glob_trans.rotation2d();
        let spread = stats.projectile_spread * 0.5;
        let size = stats.projectile_size * 0.5;
        let speed = stats.projectile_speed;
        let mut projs: Vec<Entity> = Vec::new();
        for _ in 0..stats.projectile_count {
            let proj_dir = proj_dir + thread_rng().gen_range(-spread..=spread);
            let speed =
                speed - thread_rng().gen_range(0.0..=stats.projectile_speed_variation * speed);
            let mut proj_trans = Transform::from_translation(proj_pos.extend(0.0));
            let vel = Vec2::from_angle(proj_dir) * speed;
            proj_trans.set_rotation2d(proj_dir);
            let proj = commands
                .spawn((BasicProjBundle {
                    transform: proj_trans,
                    collider: Collider::ball(size),
                    lin_vel: LinearVelocity(vel),
                    owned_by: OwnedBy(ent),
                    player_id: pid.clone(),
                    ..Default::default()
                },))
                .id();
            projs.push(proj);
        }
        // enforce fire rate
        gun.fire_wait = stats.fire_delay;
        // consume ammo and reload if empty
        if gun.rounds > 0 {
            gun.rounds -= 1;
        }
        if gun.rounds <= 0 {
            gun.reload(stats);
        }
        // apply recoil to owner
        if let Ok(mut vel) = owner_vel_query.get_mut(owner.0) {
            let dir = Vec2::from_angle(glob_trans.rotation2d());
            vel.0 += dir * -stats.recoil;
        }
        // send weapon shot event
        events.send(WeaponShot {
            weapon: ent,
            projectiles: projs,
        });
    }
}

fn handle_gun_shot(
    mut commands: Commands,
    mut events: EventReader<WeaponShot>,
    gun_query: Query<
        (
            Option<&LinearDamping>,
            Option<&DynamicGravity>,
            Option<&GravityScale>,
            Option<&Lifetime>,
            Option<&Damage>,
            Option<&Knockback>,
            Option<&Bounce>,
            Option<&BasicProjectileGfx>,
        ),
        With<Weapon>,
    >,
) {
    for shot in events.read() {
        let Ok((op_damp, op_grav, op_grav_scl, op_life, op_dmg, op_knockback, op_bounce, op_gfx)) =
            gun_query.get(shot.weapon)
        else {
            continue;
        };
        for proj in &shot.projectiles {
            let mut ent_cmd = commands.entity(*proj);
            if let Some(damping) = op_damp {
                ent_cmd.insert(damping.clone());
            }
            if let Some(dyn_grav) = op_grav {
                ent_cmd.insert(dyn_grav.clone());
            }
            if let Some(grav_scl) = op_grav_scl {
                ent_cmd.insert(grav_scl.clone());
            }
            if let Some(life) = op_life {
                ent_cmd.insert(life.clone());
            }
            if let Some(dmg) = op_dmg {
                ent_cmd.insert(dmg.clone());
            }
            if let Some(knockback) = op_knockback {
                ent_cmd.insert(knockback.clone());
            }
            if let Some(bounce) = op_bounce {
                ent_cmd.insert(bounce.clone());
            }
            if let Some(gfx) = op_gfx {
                ent_cmd.insert(gfx.clone());
            }
        }
    }
}

// Basic Gun Stats: --------------------------------------------------------------------------------

#[derive(Component, Debug, Clone)]
pub struct BasicGunStats {
    /// if true, the weapon must be triggered exactly once for each individual shot
    pub semi_automatic: bool,
    /// the amount of time, in seconds, in between shots
    pub fire_delay: f32,
    /// the amount of time it takes to reload the weapon
    pub reload_time: f32,
    /// the amount of times the weapon can be fired before reloading
    pub clip_rounds: u32,
    /// how much projectiles can spread from the weapon's aim, in radians
    pub projectile_spread: f32,
    /// how many projectiles are released when the weapon is fired
    pub projectile_count: u32,
    /// how quickly the projectiles travel when initially fired, in units/sec
    pub projectile_speed: f32,
    /// how much the speed can vary between each projectile that's fired, in units/sec
    pub projectile_speed_variation: f32,
    /// how large the projectile is
    pub projectile_size: f32,
    pub recoil: f32,
}

impl Default for BasicGunStats {
    fn default() -> Self {
        Self {
            semi_automatic: false,
            fire_delay: 0.2,
            reload_time: 1.5,
            clip_rounds: 12,
            projectile_spread: 0.075,
            projectile_count: 1,
            projectile_speed: 50.0,
            projectile_speed_variation: 0.1,
            projectile_size: 0.2,
            recoil: 0.0,
        }
    }
}

impl BasicGunStats {
    pub fn shotgun() -> Self {
        Self {
            clip_rounds: 2,
            semi_automatic: true,
            projectile_count: 14,
            projectile_speed: 150.0,
            projectile_spread: 0.35,
            projectile_speed_variation: 0.3,
            fire_delay: 0.35,
            reload_time: 2.0,
            recoil: 15.0,
            ..Default::default()
        }
    }
    pub fn machine_gun() -> Self {
        Self {
            clip_rounds: 42,
            projectile_speed: 180.0,
            projectile_spread: 0.125,
            projectile_speed_variation: 0.1,
            fire_delay: 0.08,
            reload_time: 2.5,
            recoil: 1.0,
            ..Default::default()
        }
    }
    pub fn submachine_gun() -> Self {
        Self {
            clip_rounds: 30,
            projectile_speed: 110.0,
            projectile_spread: 0.2,
            projectile_speed_variation: 0.3,
            fire_delay: 0.04,
            reload_time: 1.0,
            ..Default::default()
        }
    }
    pub fn rifle() -> Self {
        Self {
            clip_rounds: 4,
            semi_automatic: true,
            projectile_spread: 0.01,
            projectile_speed: 280.0,
            projectile_size: 0.35,
            fire_delay: 0.45,
            reload_time: 1.5,
            recoil: 10.0,
            ..Default::default()
        }
    }
}

// Gun Presets: ------------------------------------------------------------------------------------

#[derive(Bundle, Debug, Clone)]
pub struct ShotgunBundle {
    pub basic: BasicGunBundle,
    damping: LinearDamping,
    damage: Damage,
    knockback: Knockback,
    gravity: DynamicGravity,
    gravity_scl: GravityScale,
}

impl Default for ShotgunBundle {
    fn default() -> Self {
        Self {
            basic: BasicGunBundle {
                stats: BasicGunStats::shotgun(),
                ..Default::default()
            },
            damping: LinearDamping(3.5),
            damage: Damage(0.75),
            knockback: Knockback(3.0),
            gravity: Default::default(),
            gravity_scl: GravityScale(4.5),
        }
    }
}

#[derive(Bundle, Debug, Clone)]
pub struct MachineGunBundle {
    pub basic: BasicGunBundle,
    damping: LinearDamping,
    damage: Damage,
    knockback: Knockback,
    gravity: DynamicGravity,
    gravity_scl: GravityScale,
    bounce: Bounce,
}

impl Default for MachineGunBundle {
    fn default() -> Self {
        Self {
            basic: BasicGunBundle {
                stats: BasicGunStats::machine_gun(),
                ..Default::default()
            },
            damping: LinearDamping(1.0),
            damage: Damage(0.9),
            knockback: Knockback(3.0),
            gravity: Default::default(),
            gravity_scl: GravityScale(3.0),
            bounce: Bounce {
                bounce_damping: 0.25,
                bounce_threshold: 0.25,
                max_bounces: 1,
                ..Default::default()
            }
        }
    }
}

#[derive(Bundle, Debug, Clone)]
pub struct SubmachineGunBundle {
    pub basic: BasicGunBundle,
    damping: LinearDamping,
    damage: Damage,
}

impl Default for SubmachineGunBundle {
    fn default() -> Self {
        Self {
            basic: BasicGunBundle {
                stats: BasicGunStats::submachine_gun(),
                ..Default::default()
            },
            damping: LinearDamping(1.0),
            damage: Damage(0.65),
        }
    }
}

#[derive(Bundle, Debug, Clone)]
pub struct RifleBundle {
    pub basic: BasicGunBundle,
    damage: Damage,
    knockback: Knockback,
    bounce: Bounce,
}

impl Default for RifleBundle {
    fn default() -> Self {
        Self {
            basic: BasicGunBundle {
                stats: BasicGunStats::rifle(),
                ..Default::default()
            },
            damage: Damage(6.0),
            knockback: Knockback(35.0),
            bounce: Bounce {
                bounce_damping: 0.5,
                bounce_threshold: 0.9,
                max_bounces: 2,
                min_speed: 50.0,
                ..Default::default()
            },
        }
    }
}
