use std::fmt::Debug;

use bevy::input::mouse::MouseMotion;
use bevy::{input::InputSystem, prelude::*, utils::HashMap};

use super::action::{Action, Scalar};
use super::ControllerSystem;

// Player Input System Set: ------------------------------------------------------------------------

/// a system set that runs in preupdate after [`bevy::input::InputSystem`], a more
/// precise subset of [`ControllerSystem`] which only contains systems that are dealing directly
/// with input in a way directly related to the [`crate::player`] module
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub(crate) struct PlayerInputSystem;

// Player Id: --------------------------------------------------------------------------------------

pub const PLAYER_NONE: usize = 255;

#[derive(Component, Default, Debug, Clone, Copy, PartialEq, Eq)]
pub struct PlayerId(pub usize);

// Player Plugin: ----------------------------------------------------------------------------------

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(PreUpdate, ControllerSystem.after(InputSystem))
            .insert_resource(Players::default())
            .add_systems(
                PreUpdate,
                handle_player_input
                    .in_set(PlayerInputSystem)
                    .in_set(ControllerSystem),
            );
    }
}

// Current Players: --------------------------------------------------------------------------------

/// a resource used to hold the set of all current players in the game
#[derive(Resource, Debug, Clone)]
pub struct Players {
    pub current_players: Vec<Player>,
}

impl Default for Players {
    fn default() -> Self {
        Self {
            current_players: vec![Player::default()],
        }
    }
}

/// a container used to store all the necessary information about a user currently playing the game
#[derive(Default, Clone)]
pub struct Player {
    pub id: usize,
    pub input_config: PlayerInputConfig,
    current_actions: Vec<Action>,
}

impl Debug for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Player").field("id", &self.id).finish()
    }
}

impl Player {
    pub fn new(id: usize) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
    pub fn current_actions(&self) -> &Vec<Action> {
        &self.current_actions
    }
}

// Player Input: -----------------------------------------------------------------------------------

/// information about what inputs correspond to what actions for the player to make
#[derive(Debug, Clone)]
pub struct PlayerInputConfig {
    pub bindings: HashMap<PlayerInput, Vec<Action>>,
}

impl Default for PlayerInputConfig {
    fn default() -> Self {
        Self {
            bindings: HashMap::from([
                (
                    PlayerInput::Key(KeyCode::D),
                    vec![Action::MoveHorizontal(1.0.into())],
                ),
                (
                    PlayerInput::Key(KeyCode::A),
                    vec![Action::MoveHorizontal((-1.0).into())],
                ),
                (
                    PlayerInput::Key(KeyCode::W),
                    vec![Action::MoveVertical(1.0.into())],
                ),
                (
                    PlayerInput::Key(KeyCode::S),
                    vec![Action::MoveVertical((-1.0).into())],
                ),
                (PlayerInput::Key(KeyCode::Space), vec![Action::Jump]),
                (PlayerInput::Key(KeyCode::E), vec![Action::Interact]),
                (
                    PlayerInput::MouseButton(MouseButton::Left),
                    vec![Action::Attack],
                ),
            ]),
        }
    }
}

/// a type used to describe a single input that can be linked to an action
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum PlayerInput {
    Key(KeyCode),
    MouseButton(MouseButton),
}

// Systems: ----------------------------------------------------------------------------------------

/// adds current actions to the players based on key/mouse input
fn handle_player_input(
    mut players: ResMut<Players>,
    keyboard_input: Res<Input<KeyCode>>,
    mouse_btn_input: Res<Input<MouseButton>>,
    mut mouse_motion_evts: EventReader<MouseMotion>,
    gamepads: Res<Gamepads>,
    gamepad_btn_input: Res<Input<GamepadButton>>,
    gamepad_axis_input: Res<Axis<GamepadAxis>>,
) {
    let mouse_sensitivity = 0.005;
    let mut mouse_delta = Vec2::new(0.0, 0.0);
    for evt in mouse_motion_evts.read() {
        mouse_delta += evt.delta;
    }

    // iterate through each player in the game
    for player in &mut players.current_players {
        player.current_actions.clear();

        let mut kb_in = true;
        for pad in gamepads.iter() {
            // the player uses gamepad input
            if pad.id == player.id {
                kb_in = false;
                let mov = Vec2::new(
                    gamepad_axis_input
                        .get(GamepadAxis::new(pad, GamepadAxisType::LeftStickX))
                        .map_or(0.0, |i| i),
                    gamepad_axis_input
                        .get(GamepadAxis::new(pad, GamepadAxisType::LeftStickY))
                        .map_or(0.0, |i| i),
                )
                .normalize_or_zero();
                let mut aim = Vec2::new(
                    gamepad_axis_input
                        .get(GamepadAxis::new(pad, GamepadAxisType::RightStickX))
                        .map_or(0.0, |i| i),
                    gamepad_axis_input
                        .get(GamepadAxis::new(pad, GamepadAxisType::RightStickY))
                        .map_or(0.0, |i| i),
                );
                let fire = gamepad_btn_input
                    .pressed(GamepadButton::new(pad, GamepadButtonType::RightTrigger2));
                if aim.length() <= 0.1 {
                    aim = mov;
                }
                let move_x = Action::MoveHorizontal(Scalar(mov.x));
                let move_y = Action::MoveVertical(Scalar(mov.y));
                let aim_x = Action::AimHorizontal(Scalar(aim.x));
                let aim_y = Action::AimVertical(Scalar(aim.y));
                player.current_actions.push(move_x);
                player.current_actions.push(move_y);
                player.current_actions.push(aim_x);
                player.current_actions.push(aim_y);
                let jumping = gamepad_btn_input
                    .pressed(GamepadButton::new(pad, GamepadButtonType::South))
                    || gamepad_btn_input
                        .pressed(GamepadButton::new(pad, GamepadButtonType::LeftTrigger2));

                if jumping {
                    player.current_actions.push(Action::Jump);
                }
                if fire {
                    player.current_actions.push(Action::Attack);
                }
            }
        }

        // the player uses keyboard input
        if kb_in {
            // add all actions from keyboard input bindings
            for key in keyboard_input.get_pressed() {
                if let Some(actions) = player.input_config.bindings.get(&PlayerInput::Key(*key)) {
                    for action in actions {
                        player.current_actions.push(*action);
                    }
                }
            }
            // add all actions from mouse button bindings
            for btn in mouse_btn_input.get_pressed() {
                if let Some(actions) = player
                    .input_config
                    .bindings
                    .get(&PlayerInput::MouseButton(*btn))
                {
                    for action in actions {
                        player.current_actions.push(*action);
                    }
                }
            }
            player.current_actions.push(Action::AimHorizontal(Scalar(
                mouse_delta.x * mouse_sensitivity,
            )));
            player.current_actions.push(Action::AimVertical(Scalar(
                -mouse_delta.y * mouse_sensitivity,
            )));
        }
    }
}
