use crate::Hero;

use super::player::PlayerId;
use bevy::{prelude::*, transform::TransformSystem};
use bevy_xpbd_2d::prelude::*;

// Player Cam Plugin: ------------------------------------------------------------------------------

pub struct PlayerCamPlugin;

impl Plugin for PlayerCamPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PostUpdate,
            control_cameras.after(TransformSystem::TransformPropagate),
        );
    }
}

// Player Cam Controller Component: ----------------------------------------------------------------

#[derive(Component, Clone, Copy)]
pub struct PlayerCamController {
    /// the player that the camera should follow
    pub player_id: usize,
    /// how quickly the camera moves from it's current position to it's  target
    pub follow_factor: f32,
    /// how much the velocity effects the camera movement
    pub velocity_factor: f32,
    prev_position: Vec2,
}

impl Default for PlayerCamController {
    fn default() -> Self {
        Self {
            player_id: default(),
            follow_factor: 0.05,
            velocity_factor: 0.5,
            prev_position: default(),
        }
    }
}

impl PlayerCamController {
    pub fn new(id: usize) -> Self {
        Self {
            player_id: id,
            ..Default::default()
        }
    }
}

/// a system to control the player cameras based on the players in game
pub fn control_cameras(
    time: Res<Time>,
    mut cam_query: Query<(
        &mut Transform,
        &mut PlayerCamController,
        &mut OrthographicProjection,
        &Camera,
    )>,
    player_query: Query<(&PlayerId, &GlobalTransform, Option<&LinearVelocity>), With<Hero>>,
) {
    let dt = time.delta_seconds();
    for (mut cam_trans, mut cam_ctrl, mut ortho, cam) in &mut cam_query {
        let mut first_iter = true;
        let mut target_count = 0;
        let mut target_vel = Vec2::ZERO;
        let spacing = 15.0;
        let mut min_trans = Vec2::ZERO;
        let mut max_trans = Vec2::ZERO;
        for (pid, hero_glob_trans, hero_vel) in &player_query {
            // we only want to pay attention to heros of the same player id as the cam controller
            if pid.0 != cam_ctrl.player_id && cam_ctrl.player_id != 255 {
                continue;
            }
            target_count += 1;
            let hero_trans = hero_glob_trans.translation().truncate();
            if first_iter {
                min_trans = hero_trans;
                max_trans = hero_trans;
                first_iter = false;
            }
            min_trans.x = min_trans.x.min(hero_trans.x - spacing);
            min_trans.y = min_trans.y.min(hero_trans.y - spacing);
            max_trans.x = max_trans.x.max(hero_trans.x + spacing);
            max_trans.y = max_trans.y.max(hero_trans.y + spacing);
            if let Some(hero_vel) = hero_vel {
                target_vel += hero_vel.0;
            }
        }
        // calculate average translation and velocity
        let target_trans = (min_trans + max_trans) * 0.5;
        target_vel /= target_count as f32;
        if target_trans.is_nan() {
            continue;
        }
        let vp_size = cam.logical_viewport_size().unwrap_or(Vec2::ONE);
        let target_size = max_trans - min_trans;
        let scl = (target_size.x / vp_size.x).max(target_size.y / vp_size.y);
        // begin calculating final camera position
        let target_pos = target_trans + target_vel * cam_ctrl.velocity_factor;
        // adjust follow factor for delta time
        let follow_adj = 1.0 - (1.0 - cam_ctrl.follow_factor).powf(dt * 60.0);
        ortho.scale = (ortho.scale * (1.0 - follow_adj)) + scl * follow_adj;
        let final_pos = cam_ctrl.prev_position * (1.0 - follow_adj) + (target_pos * follow_adj);
        cam_trans.translation = final_pos.extend(cam_trans.translation.z);
        cam_ctrl.prev_position = final_pos;
    }
}

// Player Cam Bundle: ------------------------------------------------------------------------------

#[derive(Bundle, Default)]
pub struct PlayerCamBundle {
    pub player_cam_ctrl: PlayerCamController,
    pub cam_bundle: Camera2dBundle,
}
