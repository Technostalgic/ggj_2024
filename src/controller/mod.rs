pub mod action;
pub mod player;
pub mod player_cam;

use bevy::prelude::*;

// System Sets: ------------------------------------------------------------------------------------

/// system set that runs in preupdate directly after [`bevy::input::InputSystem`], which deals
/// with handling player input from mouse, keyboard, controllers, etc
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct ControllerSystem;

// Controller Plugin: ------------------------------------------------------------------------------

pub struct ControllerPlugin;

impl Plugin for ControllerPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((
            action::ActionPlugin,
            player::PlayerPlugin,
            player_cam::PlayerCamPlugin,
        ));
    }
}
